import './App.css';
import './App2.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { getUserFromToken, isTeacher } from './services/auth-services';
import Login from './components/pages/Login';
import AuthContext from './providers/AuthContext';
import Register from './components/pages/Register';
import ErrorBoundary from './components/utility/ErrorBoundary';
import CreateCourse from './components/pages/CreateCourse';
import Home from './components/pages/Home';
import Layout from './components/layout/Layout';
import ManageCourse from './components/pages/ManageCourse';
import EditProfile from './components/pages/EditProfile';
import GuardedRoute from './providers/GuardedRoute';
import { MyCoursesProvider } from './hooks/myCoursesContext';
import CoursePage from './components/pages/CoursePage';
import SectionPage from './components/pages/SectionPage';
import NotFound from './components/pages/NotFound';
import NotAuthorized from './components/pages/NotAuthorized';


function App() {

  const [user, setLoggedUser] = useState(getUserFromToken(localStorage.getItem('token')));
  // to be implemented when exp.date of the token is set in the payload
  // useEffect(() => {

  //   if(!user) return;
  //   const timer = setTimeout(() => {
  //     logoutUser(setLoggedUser);
  //   }, new Date(user?.exp * 1000 || 0) - new Date());
  //   return () => clearTimeout(timer);
  // }, [user]);

  return (
    <BrowserRouter>
      <ErrorBoundary>
        <AuthContext.Provider value={{user, setLoggedUser}}>
          <MyCoursesProvider user={user}>            
            <Layout>
              <Switch>
                <Redirect path="/" exact to="/home" />                
                <Route path="/home" component={Home} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <GuardedRoute path="/profile" exact component={EditProfile} auth={!!user} toPath="/login" authorized={!!user} toAuthorizationPath="/home" />
                <GuardedRoute path="/courses/create" exact component={CreateCourse} auth={!!user} toPath="/login" authorized={isTeacher()} toAuthorizationPath="/home" /> 
                <GuardedRoute path="/courses/:id" exact component={CoursePage} auth={!!user} toPath="/login" authorized={!!user} toAuthorizationPath="/home" />                
                <GuardedRoute path="/courses/:id/manage" exact component={ManageCourse} auth={!!user} toPath="/login" authorized={isTeacher()} toAuthorizationPath="/home" />       
                <GuardedRoute path="/courses/:id/sections/:sid" exact component={SectionPage} auth={!!user} toPath="/login" authorized={!!user} toAuthorizationPath="/home" /> 
                <Route path="/forbidden" component={NotAuthorized} />
                <Route path="*" component={NotFound} />
              </Switch>                
            </Layout>
          </MyCoursesProvider>
        </AuthContext.Provider>
      </ErrorBoundary>
    </BrowserRouter>
  );
}

export default App;
