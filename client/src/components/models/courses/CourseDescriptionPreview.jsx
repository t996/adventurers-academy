import React from 'react';
import PropTypes from 'prop-types';
import CourseDescription from './CourseDescription';
import CourseListItem from './CourseListItem';


const CourseDescriptionPreview = ({ course }) => {

  return (
    <div>
            
      <div style={{ display: 'flex', flexDirection: 'row', paddingTop: '2rem'}}>
        <div style={{ display: 'flex', flexDirection: 'column', textAlign: 'center', marginRight: '5rem'}}>
          <div style={{fontWeight: 'bold', marginTop: '1rem', fontSize: '1.4rem'}}> {course.isPrivate ? 'PRIVATE course' : 'PUBLIC course'}</div>
          <CourseListItem course={course} onClick={() => {}} />
        </div>
      
        <CourseDescription course={course} hideActions={true} />  
      </div>
    </div>
  );
};

CourseDescriptionPreview.propTypes = {
  course: PropTypes.object.isRequired,  
};

export default CourseDescriptionPreview;