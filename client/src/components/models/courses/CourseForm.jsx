import React, { useState } from 'react';
import PropTypes from 'prop-types';
import AceEditor from 'react-ace';
import 'ace-builds/webpack-resolver';
import 'ace-builds/src-noconflict/mode-html';
import 'ace-builds/src-noconflict/theme-solarized_light';
import 'ace-builds/src-min-noconflict/ext-searchbox';
import 'ace-builds/src-min-noconflict/ext-language_tools';
import { useForm, Controller } from 'react-hook-form';
import CourseListItem from './CourseListItem';
import CourseDescription from './CourseDescription';
import { validateTitle } from '../../../services/http-course-requests';


const CourseForm = ({ course, onSubmit, onCancel }) => {  
    
  const { register, handleSubmit, watch, formState: { errors }, control } = useForm();
  const [error, setError] = useState(null);
  
  const title = watch('title', course ? course.title : '');
  const description = watch('description', course ? course.description : '');

  const tempCourse = {
    id: course ? course.id : 0,
    title,
    description,
  };
  
  const titleIsUnique = async (title) => {
    
    if (course && title === course.title) {
      return true; // fast check: the title was not changed so it must be unique
    }
    
    try {
      const result = await validateTitle(course ? course.id : undefined, title);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      return result;
    }
    catch(err) {
      setError(err);
    }
  };

  if (error) throw error;
  
 
  return (
    <div className="course-form-container">
      <div className="course-form-division">
        <form className="course-form" onSubmit={handleSubmit(onSubmit)}>
          <div style={{ display: 'flex' }}>
            <input
              style={{ flex: 1, marginRight: '0.25rem' }}
              type="text"
              name="title"
              placeholder="Course Title"
              className={`academy-field${errors.title ? ' not-valid-field' : ''}`}
              defaultValue={course ? course.title : ''} 
              {...register('title', { 
                required: { value: true, message: 'Title is required' },
                minLength: { value: 5, message: 'Title must be at least 5 chars' }, 
                maxLength: { value: 100, message: 'Title must be less than 100 chars' },
                validate: titleIsUnique
              })}
            />
            
            <label className="academy-field" >
              <input
                type="checkbox"
                name="isPrivate"
                defaultChecked={ course ? course.isPrivate : false}
                {...register('isPrivate')}
              />
              Private
            </label>
          </div>
          <Controller
            name="description"
            control={control}
            defaultValue={course ? course.description : ''}
            render={({ field }) => 
              <AceEditor
                style={{ 
                  width: '30rem', 
                  height: '32rem', 
                  borderRadius: '0.30rem', 
                  opacity: 0.85, 
                  borderWidth: '0.15rem', 
                  borderColor: '#c69842',
                  borderStyle: 'solid',
                  color: 'black',
                }}
                {...field} 
                mode="html"
                theme="solarized_light"
                name="some_unique_id"
                showGutter={true}
                highlightActiveLine={true}
                wrapEnabled={true}
                editorProps={{ $blockScrolling: true }}
                setOptions={{
                  enableBasicAutocompletion: true,
                  enableLiveAutocompletion: true,
                  enableSnippets: true,
                  showLineNumbers: true,
                  tabSize: 2,
                }}
              />}           
          />
       
          <div className="form-buttons">
            {errors.title && (<div className="error-message">{errors.title.message}</div>)}
            {errors.title && errors.title.type === 'validate' && (<div className="error-message">The same title is in use</div>)}

            <button type="submit" className="academy-button">
              {course ? 'Update' : 'Create'}
            </button>
            
            <button type="button" onClick={onCancel} className="academy-button">
              Cancel
            </button>
          </div>

        </form>  
      </div>
      <div className="course-form-division" style={{ flex: 1, paddingLeft: '0.5rem', paddingTop: '2.5rem'  }}>
        <CourseDescription course={tempCourse} hideActions={true}>
          <CourseListItem course={tempCourse} onClick={() => {}} />
        </CourseDescription>
      </div>
    </div>
  );
};

CourseForm.propTypes = {
  course: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,  
};

export default CourseForm;
