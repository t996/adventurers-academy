import { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory, NavLink } from 'react-router-dom';
import AuthContext from '../../../providers/AuthContext';
import { isTeacher } from '../../../services/auth-services';
import SpinnerSmall from '../../general/SpinnerSmall';
import { useMyCoursesData } from '../../../hooks/myCoursesContext';
import { enrollInCourse } from '../../../services/http-course-requests';


const CourseDescription = ({ course, hideActions, ...props }) => {
  const { user } = useContext(AuthContext);
  const { dispatch } = useMyCoursesData();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const history = useHistory();
 
 
  const handleView = () => {
    history.push(`/courses/${course.id}`);
  };

  const handleEnroll = async () => {
    
    setLoading(true);
    
    try {
      const result = await enrollInCourse(course.id);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      dispatch({ type: 'init', data: result });      
      
      setLoading(false);
      history.push(`/courses/${course.id}`);
    }
    catch(err) {
      setError(err);
      setLoading(false);
    }   
  };

  if (error) throw error;

  return (
    <div className="course-description-panel" 
      style={{ 
        backgroundImage: `url(${process.env.PUBLIC_URL + '/images/description-open-book.jpg'})`}}>

      <div className="first-page">
        {props.children 
          ? props.children
          : <>
            <div className="title">
              {course.title}
            </div>
            {!hideActions 
              ? <div className="actions">
                {!user
                  ? <div>
                      In order to see the content of this course you must be logged in.
                    <br /> 
                      You can <NavLink to="/login">[Login]</NavLink> or <NavLink to="/register">[Register]</NavLink>
                  </div>
                  : isTeacher() 
                    ? <button onClick={handleView} className="academy-button">View</button> 
                    : loading? <SpinnerSmall /> : <button onClick={handleEnroll} className="academy-button">Enroll</button>
                }
              </div>
              : null
            }
          </>
        }
      </div>

      <div className="second-page">
        <div className="description" dangerouslySetInnerHTML={{ __html: course.description}} />
      </div>
    </div>
  );
};

CourseDescription.propTypes = {
  course: PropTypes.object.isRequired,
  hideActions: PropTypes.bool.isRequired, 
};

export default CourseDescription;