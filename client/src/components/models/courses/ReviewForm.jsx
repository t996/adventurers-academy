import React from 'react';
import PropTypes from 'prop-types';
import { useForm, FormProvider } from 'react-hook-form';
import { useCourseData } from '../../../hooks/courseContext';
import CheckboxList from '../../general/CheckboxList';


const ReviewForm = ({ onSubmit }) => {
  const { courseData: course } = useCourseData();  
  const methods = useForm();
  
  const branchListItems = course.branches.map(branch => {
    return {
      id: branch.id,
      name: branch.title + (branch.isPrivate ? ' (private)' : ''),
      checked: !branch.isPrivate,
    };
  });

  const branchListDefaultValue = branchListItems.reduce((acc, item) => item.checked ? [...acc, item.id] : acc ,[]);
 
  return (  
    <FormProvider {...methods}>
      <form className="review-form" onSubmit={methods.handleSubmit(onSubmit)}>
        
        <label className="form-check-label academy-field">
          <input
            className="form-check-input" 
            type="checkbox"
            defaultChecked={false}
            {...methods.register('unlockAll')}
          />
          Unlock all sections
        </label>
        
        
        <CheckboxList  
          name="branchIds"
          id="branchIds"
          value={branchListDefaultValue}
          listItems={branchListItems} 
        />

        {/* <Controller
          name="branchIds"
          control={control}
          defaultValue={branchListDefaultValue}
          render={({ field }) => <CheckboxList2
            {...field}  
            name="branchesCBL_1"
            id="branchesCBL"
            listItems={branchListItems} 
          />}
        />    */}

        <button type="submit" className="academy-button">Review</button>
      </form>
    </FormProvider>   
  );
};

ReviewForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default ReviewForm;
