import React, { useState } from 'react';
import { useCourseData } from '../../../hooks/courseContext';
import ReviewCourse from './ReviewCourse';
import ReviewForm from './ReviewForm';


const initFilter = (course) => {

  const branchIds = course.branches.reduce((acc, item) => item.isPrivate ? acc : [...acc, item.id] ,[]);
  return {
    unlockAll: false,
    branchIds,
  };
};

const ReviewTabPanel = () => {
  const { courseData: course } = useCourseData();
  const [filter, setFilter] = useState(initFilter(course));
  
  const review = (form) => {
    setFilter(form);
  };
  
  return (
    <div className="react-tabs-content" style={{ flexDirection: 'column' }}>     
      <ReviewForm onSubmit={review} />
     
      <ReviewCourse filter={filter} />
    </div>
  );
};

export default ReviewTabPanel;