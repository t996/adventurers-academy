
import React from 'react';
import PropTypes from 'prop-types';
import { useMyCoursesData } from '../../../hooks/myCoursesContext';


const CourseListItem = ({ course, onClick }) => {
  
  const { courseData: courses, loading } = useMyCoursesData();
  
  const getBookCover = (id) => {
    return process.env.PUBLIC_URL + `/images/covers/book-cover-md-0${id % 10}.png`;
  };

  let isInMyCourses = false;
  
  if (!loading && courses && courses.length > 0) {
    isInMyCourses = !!courses.find(c => c.id === course.id);
  } 
  
  return (
    <div 
      style={{ backgroundImage: `url(${getBookCover(course.id)})`}}
      className="col-lg-3 col-sm-6 col-md-4 col-xs-1 single-course"
      onClick={e => onClick(course)}
    >
      <div className='front-page-title'>{course.title}</div>
      {isInMyCourses && <div className='front-page-progress'>In Progress...</div>}
    </div>
  );
};

CourseListItem.propTypes = {
  course: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,  
};

export default CourseListItem;