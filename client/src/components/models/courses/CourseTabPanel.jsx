import React, { useState } from 'react';
import { useCourseData } from '../../../hooks/courseContext';
import { /*deleteCourse,*/ updateCourse } from '../../../services/http-course-requests';
import CourseForm from './CourseForm';
import CourseDescriptionPreview from './CourseDescriptionPreview';


const CourseTabPanel = () => {

  const { courseData: course, dispatch } = useCourseData();
  const [editMode, setEditMode] = useState(false);
  const [error, setError] = useState(null);
  // const [showConfirm, setShowConfirm] = useState(false);


  const update = async (courseForm) => {
    const data = {
      title: courseForm.title,
      description: courseForm.description,
      isPrivate: courseForm.isPrivate,
    };

    try {
      const result = await updateCourse(course.id, data);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      dispatch({ type: 'edit', data: result });
      setEditMode(false);      
    }
    catch(err) {
      setError(err);
    }
  };
  
  // const remove = async () => {
    
  //   try {
  //     const result = await deleteCourse(course.id);
      
  //     if (result.error) {
  //       throw new Error(result.message);
  //     }
      
  //     dispatch({ type: 'edit', data: result });
  //     setEditMode(false);
  //   }
  //   catch(err) {
  //     setError(err);
  //   }
  // };

  // const handleDelete = () => {
  //   setShowConfirm(true);
  // };

  if (error) throw error;

  return (
    <div className="react-tabs-content">      
      {editMode 
        ? <CourseForm course={course} onSubmit={update} onCancel={() => setEditMode(false)} />
        : <div className="react-tabs-inner-content">
          <div className='action-button-container'>
            <button className="academy-button" onClick={() => setEditMode(true)}>Edit Course</button>
            {/* <button className="academy-button" onClick={handleDelete}>Delete Course</button> */}
          </div>
          <CourseDescriptionPreview course={course} /> 
        </div>        
      }  
    </div>
  );
};

export default CourseTabPanel;