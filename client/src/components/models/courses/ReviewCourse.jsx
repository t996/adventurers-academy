import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useCourseData } from '../../../hooks/courseContext';
import { getCourseBranchSections } from '../../../services/http-course-requests';
import Spinner from '../../general/Spinner';
import SectionView from '../sections/SectionView';


const ReviewCourse = ({ filter }) => {

  const { courseData: course } = useCourseData();
  const [branchSections, setBranchSections] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setIsLoading] = useState(true);   
  
  useEffect(() => {
    let unmounted = false;

    setIsLoading(true);
    setError(null);

    const getData = async () => {

      try {
        const result = await getCourseBranchSections(course.id, filter);
        
        if (result.error) {
          throw new Error(result.message);
        }
        
        if (!unmounted) {
          setBranchSections(result);
          setIsLoading(false);
        }        
      }
      catch(err) {
        if (!unmounted) {
          setError(err);
          setIsLoading(false);
        }
      }
    };

    getData();
    return () => { unmounted = true; };
  }, [course, filter]);

  if (error) throw error;
  if (loading) return <Spinner />;

  return (
    <div className="course-view">
      <div className="course-sections-view" >
        <div className="course-view-title">
          <div className="section-top-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-01.png'})`}} ></div>
          <div className="course-title">
            {course ? <h2>{course.title}</h2> : ''}
          </div>
          {course && <div className="section-bottom-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-02.png'})`}} ></div>}
        </div>
        {branchSections.map(bs => <SectionView key={bs.section.id} branchSection={bs} courseId={course.id} disableLinks={true} ignoreDates={filter.unlockAll} />)}
      </div>
    </div>
  );
};

ReviewCourse.propTypes = {
  filter: PropTypes.object.isRequired, 
};

export default ReviewCourse;