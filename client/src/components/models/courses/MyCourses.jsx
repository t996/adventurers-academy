import CourseList from './CourseList';
import { useMyCoursesData } from '../../../hooks/myCoursesContext';
import { useHistory } from 'react-router';
import Spinner from '../../general/Spinner';


const MyCourses = () => {  

  const { courseData: courses, loading } = useMyCoursesData();
  const history = useHistory();

  const handleClick = (course) => {
    history.push(`/courses/${course.id}`);
  };

  if(loading) {
    return <Spinner />;
  }

  return (    
    <>
      {courses.length === 0 
        ? <div className="no-courses-found">You are not enrolled in any courses!</div>
        : <CourseList courses={courses} onClick={handleClick} />        
      }
    </>
  );
};

export default MyCourses;
