import React from 'react';
import PropTypes from 'prop-types';
import CourseListItem from './CourseListItem';


const CourseList = ({ courses, onClick }) => {
  
  return (
    <div className="row public-courses">
      {courses.map((c) => <CourseListItem key={c.id} course={c} onClick={onClick} />)}
    </div>  
  );
};

CourseList.propTypes = {
  courses: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,  
};

export default CourseList;