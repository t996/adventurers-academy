import React from 'react';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';


const Section = ({ section, onClick }) => {

  const renderTooltip = props => (

    <Tooltip className="section-title-tooltip" {...props}>{section.title}</Tooltip>

  );

  return (
    <OverlayTrigger placement="top" overlay={renderTooltip}>
      <div className="section-small-panel" onClick={() => onClick(section)} >
        <div className="body">
          {section.title}
        </div>
        <div className="footer">
          {section.isEmbedded ? 
            <>
              {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" fill="currentColor" className="bi bi-file-code" viewBox="0 0 16 16">
                <path d="M6.646 5.646a.5.5 0 1 1 .708.708L5.707 8l1.647 1.646a.5.5 0 0 1-.708.708l-2-2a.5.5 0 0 1 0-.708l2-2zm2.708 0a.5.5 0 1 0-.708.708L10.293 8 8.646 9.646a.5.5 0 0 0 .708.708l2-2a.5.5 0 0 0 0-.708l-2-2z"/>
                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
              </svg> */}
           &lt;/&gt;&nbsp; embedded </>: ''}
        </div>       
      </div>
    </OverlayTrigger>
  );
};

Section.propTypes = {
  section: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Section;