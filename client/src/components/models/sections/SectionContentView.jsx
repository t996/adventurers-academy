import React from 'react';
import PropTypes from 'prop-types';


const SectionContentView = ({ section }) => {

  const content = section.section ? section.section.content : section.content;
  const title = section.section ? section.section.title : section.title;

  return (
    <div className='paper-background'>
      <div className="section-title"><h2>{title}</h2></div>
      <div className="section-content" dangerouslySetInnerHTML={{ __html: content }}></div>      
      <div className="section-bottom-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-02.png'})`}} ></div>
    </div>                            
  );
};

SectionContentView.propTypes = {
  section: PropTypes.object.isRequired,  
};

export default SectionContentView;