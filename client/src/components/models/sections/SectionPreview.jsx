import React from 'react';
import PropTypes from 'prop-types';


const SectionPreview = ({ section }) => {

  return (
    <div>
      <div className="section-preview-title">{section.title}</div>
      {/* is private is seen only by the teachers */}
      <div className="section-preview-private">{section.isEmbedded ? 'embedded' : 'not embedded'}</div>
      <div className="section-preview-description" dangerouslySetInnerHTML={{__html: section.content}}></div>      
    </div>
  );
};

SectionPreview.propTypes = {
  section: PropTypes.object.isRequired, 
};

export default SectionPreview;