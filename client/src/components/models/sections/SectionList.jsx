import React from 'react';
import PropTypes from 'prop-types';
// import SectionListItem from './SectionListItem';
import Section from './Section';


const SectionList = ({ branch, sections, onSelect }) => {

  return (
    <div className="section-list-container">
      {sections.length > 0
        ? <ul className="section-list">
          {sections.map((s) => branch.sections.find(bs => bs.section === s.id) ? null : <Section key={s.id} section={s} onClick={onSelect} />)}
        </ul>
        : <h2>No sections</h2>
      }
    </div>
  );
};

SectionList.propTypes = {
  branch: PropTypes.object.isRequired,
  sections: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,  
};

export default SectionList;