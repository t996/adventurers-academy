import React from 'react';
import PropTypes from 'prop-types';
import AceEditor from 'react-ace';
import 'ace-builds/webpack-resolver';
import 'ace-builds/src-noconflict/mode-html';
import 'ace-builds/src-noconflict/theme-cobalt';
import 'ace-builds/src-min-noconflict/ext-searchbox';
import 'ace-builds/src-min-noconflict/ext-language_tools';
import { useForm, Controller } from 'react-hook-form';


const SectionForm = ({ section, onSubmit, onCancel }) => {  
  
  const { register, handleSubmit, formState: { errors }, control } = useForm();
 
  
  return (
    <div className="section-form-container">
      <form className="section-form" onSubmit={handleSubmit(onSubmit)}>
        <div style={{ display: 'flex' }}>
          <input
            style={{ flex: 1, marginRight: '0.25rem' }}
            type="text"
            name="title"
            placeholder="Section Title"
            className={`academy-field${errors.title ? ' not-valid-field' : ''}`}
            defaultValue={section ? section.title : ''} 
            {...register('title', { 
              required: { value: true, message: 'Title is required' },
              minLength: { value: 5, message: 'Title must be at least 5 chars' }, 
              maxLength: { value: 50, message: 'Title must be less than 50 chars' },                
            })}
          />
            
          <label className="academy-field" >
            <input
              type="checkbox"
              name="isEmbedded"
              defaultChecked={ section ? section.isEmbedded : false}
              {...register('isEmbedded')}
            />
              Embedded
          </label>
        </div>
        <Controller
          name="content"
          control={control}
          defaultValue={section ? section.content : ''}
          render={({ field }) => 
            <AceEditor
              style={{ 
                width: '40rem', 
                height: '26rem', 
                borderRadius: '0.30rem', 
                opacity: 0.85, 
                borderWidth: '0.15rem', 
                borderColor: '#c69842',
                borderStyle: 'solid',
                color: 'black',
              }}
              {...field} 
              mode="html"
              theme="solarized_light"
              name="some_unique_id"
              showGutter={true}
              highlightActiveLine={true}
              wrapEnabled={true}
              editorProps={{ $blockScrolling: true }}
              setOptions={{
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true,
                enableSnippets: true,
                showLineNumbers: true,
                tabSize: 2,
              }}
            />}           
        />
       
        <div className='form-buttons'>
          {errors.title && (<div className="error-message">{errors.title.message}</div>)}
            
          <button type="submit" className="academy-button">
            {section ? 'Update' : 'Create'}
          </button>
            
          <button type="button" onClick={onCancel} className="academy-button">
              Cancel
          </button>
        </div>
      </form>      
    </div>
  );
};

SectionForm.propTypes = {
  section: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,  
};

export default SectionForm;
