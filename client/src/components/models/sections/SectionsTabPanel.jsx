import React, { useState } from 'react';
import { useCourseData } from '../../../hooks/courseContext';
import { createSection, getSection, updateSection } from '../../../services/http-section-requests';
import Spinner from '../../general/Spinner';
import Section from './Section';
import SectionContentView from './SectionContentView';
import SectionForm from './SectionForm';


const SectionsTabPanel = () => {
  const { courseData: course , dispatch } = useCourseData();
  const { sections } = course;

  const [mode, setMode] = useState('list'); // modes: 'list', 'create', 'view', 'edit'
  const [selected, setSelected] = useState(null);
  const [error, setError] = useState(null);


  const handleMode = (nextMode) => {
    setMode(nextMode);
    
    if (nextMode === 'list' || nextMode === 'create') {
      setSelected(null); //remove the selected section if any
    }
  };

  
  const select = async (id) => {
    setMode('view');
    setSelected(null); // used for loading flag too
    
    try {
      const result = await getSection(id);
        
      if (result.error) {
        throw new Error(result.message);
      }

      setSelected(result);        
    }
    catch(err) {
      setError(err);
    }
  };
  
  const update = async (form) => {
    const data = {
      title: form.title,
      content: form.content,
      isEmbedded: form.isEmbedded,
    };

    try {
      const result = await updateSection(selected.id, data);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      dispatch({ type: 'updateSection', data: result });
      setSelected(result);
      setMode('view');      
    }
    catch(err) {
      setError(err);
    }
  };

  const create = async (form) => {
    const data = {
      title: form.title,
      content: form.content,
      isEmbedded: form.isEmbedded,
      courseId: course.id,
    };


    try {
      const result = await createSection(data);
      
      if (result.error) {
        throw new Error(result.message);
      }

      dispatch({ type: 'createSection', data: result });
      setSelected(result);
      setMode('view');
    }
    catch(err) {
      setError(err);
    }
  };
  

  if (error) throw error;

  return (
    <div className="react-tabs-content">
      {mode === 'list' 
        ? <div className="react-tabs-inner-content">
          <div className='action-button-container'>
            <button className="academy-button" style={{ marginLeft: '0.5rem' }} onClick={() => handleMode('create')} >Create Section</button>
          </div>
          
          <div className="sections-container">
            {sections.map((s) => <Section key={s.id} section={s} onClick={() => select(s.id)} />)}
          </div>
        </div>
        : null
      }

      {mode === 'create'
        ? <div className="react-tabs-inner-content">
          <div className='action-button-container'>
            <button className="academy-button" onClick={() => handleMode('list')} >Go Back</button>
          </div>
          <SectionForm section={null} onCancel={() => handleMode('list')} onSubmit={create} />
        </div>
        : null
      }
      
      {mode === 'view'
        ? !selected 
          ? <Spinner /> 
          : <div className="react-tabs-inner-content">
            <div className='action-button-container'>
              <button className="academy-button" onClick={() => handleMode('list')} >Go Back</button>
              <button className="academy-button" onClick={() => handleMode('edit')} >Edit Section</button>
            </div>
            <div className="course-view">
              <div className="course-sections-view">
                <div className="section-view" >
                  <div className="section-top-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-01.png'})`}} ></div>
                  <SectionContentView section={selected}/>
                </div>
              </div>
            </div>
          </div>
        : null
      }
      
      {mode === 'edit'
        ? !selected 
          ? <Spinner /> 
          : <div className="react-tabs-inner-content">
            <div className='action-button-container'>
              <button className="academy-button" onClick={() => handleMode('view')} >Go Back</button>
            </div>
            <SectionForm section={selected} onCancel={() => handleMode('view')} onSubmit={update} />
          </div>
        : null
      }
    </div>
  );
};

export default SectionsTabPanel;