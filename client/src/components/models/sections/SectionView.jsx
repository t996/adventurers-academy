import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import SectionContentView from './SectionContentView';


const SectionView = ({ courseId, branchSection, disableLinks, ignoreDates }) => {

  return (
    <div className="section-view" >

      {branchSection.timeLock && new Date(branchSection.timeLock) > (ignoreDates ? new Date(8640000000000000) : new Date())
        ? 
        <div className='paper-background'>
          <div className="section-title"><h2>{branchSection.section.title}</h2></div>
          <div className="timelocked-section" style={{backgroundImage: `url(${process.env.PUBLIC_URL + '/images/sand-glass.png'})`}}>
              This section is locked until  {moment(branchSection.timeLock).format('YYYY-MM-DD')} 
          </div>
          <div className="section-bottom-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-02.png'})`}} ></div>
        </div>
        : branchSection.section.isEmbedded 
          ? <SectionContentView section={branchSection} />
          : <div className='paper-background'>
            <div className="section-title"><h2>{branchSection.section.title}</h2></div>
            <div className='not-embedded-section'>
              {disableLinks 
                ? <span style={{ textDecoration: 'underline'}}>Click here to see the content</span> 
                : <Link to={`/courses/${courseId}/sections/${branchSection.section.id}`}>
                  Click here to see the content
                </Link>}
            </div>
            <div className="section-bottom-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-02.png'})`}} ></div>
          </div>  
      }
    </div>
  );
};

SectionView.propTypes = {
  courseId: PropTypes.number.isRequired,
  branchSection: PropTypes.object.isRequired,
  disableLinks: PropTypes.bool.isRequired,
  ignoreDates: PropTypes.bool.isRequired,
};


export default SectionView;