import React from 'react';
import PropTypes from 'prop-types';
import SectionInSlot from '../branches/SectionInSlot';


const BranchSlot = ({ index, slot, onAdd, onLock, onDelete }) => {

  return (
    <div className="branch-slot">
      {slot 
        ? <>
          <SectionInSlot slot={slot} onDelete={() => onDelete(slot.id)} onLock={() => onLock(index)} />          
        </>
        : <div className="empty-slot" onClick={()=> onAdd(index)}>
          +
          <div className="slot-position ">
            {index + 1}
          </div>
        </div>
      }
    </div>
  ); 
};


BranchSlot.propTypes = {
  index: PropTypes.number.isRequired,
  slot: PropTypes.object,
  onAdd: PropTypes.func.isRequired,
  onLock: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default BranchSlot;