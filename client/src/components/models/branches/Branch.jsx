import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useCourseData } from '../../../hooks/courseContext';
import SectionList from '../sections/SectionList';
import BranchSlot from './BranchSlot';
import { Modal } from 'react-bootstrap';
import TimeLockForm from './TimeLockForm';
import BranchDropDown from './BranchDropDown';


const initSlots = (branch, sections, numberOfSlots) => {
  const sectionSlots = [];

  for (let i = 0; i < numberOfSlots; i++) {
    const sectionAtPosition = branch.sections.find(s => s.position === i); 
    if (sectionAtPosition) {
      const sec = sections.find(s => s.id === sectionAtPosition.section);
      sectionSlots.push({ 
        id: sec.id, 
        title: sec.title,
        isEmbedded: sec.isEmbedded,
        timeLock: sectionAtPosition.timeLock 
      });
    } else {
      sectionSlots.push(null);
    }    
  }

  return sectionSlots;
};

const Branch = ({ branch, numberOfSlots, onEdit, onUpdate, onDelete }) => {

  const { courseData: course } = useCourseData();
  const { sections } = course;
  const [modal, setModal] = useState(''); // modal: '', 'list', 'lock'
  const [selectedSlotIndex, setSelectedSlotIndex] = useState(null); 

  const handleAdd = (slotIndex) => {
    setModal('list');
    setSelectedSlotIndex(slotIndex);   
  };

  const handleLock = (slotIndex) => {
    setModal('lock');
    setSelectedSlotIndex(slotIndex);
  };

  const handleCancel = () => {
    setModal('');
    setSelectedSlotIndex(null);
  };


  const addBranchSection = (section) => {
    const selectedSection = sections.find(s => s.id === section.id);
    
    const { sections: branchSections } = branch;
    branchSections.push({
      section: selectedSection.id,
      position: selectedSlotIndex,
      timeLock: null,
    });

    const updatedBranch = { ...branch, sections: branchSections };
    
    onUpdate(updatedBranch);
    handleCancel(); // remove selectedIndex
  };

  const removeBranchSection = (sectionId) => {
    const foundIndex = branch.sections.findIndex(a => a.section === sectionId);
    branch.sections.splice(foundIndex, 1);
    const updatedBranch = { ...branch };
    onUpdate(updatedBranch);    
  };

  const updateBranchSection = (form) => {
    const { sectionId, timeLock } = form;
    const foundIndex = branch.sections.findIndex(a => a.section === +sectionId);
    branch.sections[foundIndex] = { ...branch.sections[foundIndex], timeLock };
    
    const updatedBranch = { ...branch };
    
    onUpdate(updatedBranch);
    handleCancel(); // reset selectedIndex
  };

 
  const sectionSlots = initSlots(branch, sections, numberOfSlots);
  
  return (
    <>
      <Modal
        show={modal !== ''}
        onHide={handleCancel}
        size="mg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        animation={false}
        // backdrop="static"
        // keyboard={false}
        dialogClassName="branch-modal-container"
      >      
        <Modal.Body>
          { modal === 'list' ? <SectionList branch={branch} sections={sections} onCancel={handleCancel} onSelect={addBranchSection} /> : null }
          { modal === 'lock' ? <TimeLockForm branchSection={branch.sections.find(bs => bs.position === selectedSlotIndex)} onCancel={handleCancel} onSubmit={updateBranchSection} /> : null }
        </Modal.Body>     
      
      </Modal>      
      
      <div className="branch-container">
        <div className="branch-header" >
          {branch.title}
          <div>            
            {branch.isPrivate ? 
              <>
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" className="bi bi-lock-fill" viewBox="0 0 16 16">
                  <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"/>
                </svg>
                private </>: <span>&nbsp;</span>}
          </div>
          
          <div style={{ position: 'absolute', top: 0, right: 0, marginTop: '0rem', marginRight: '0.25rem' }} >
            <BranchDropDown onEdit={onEdit} onDelete={onDelete} />
          </div>
        </div>
        <div className="section-slots">
          {sectionSlots.map((value, index) => <BranchSlot key={index} index={index} slot={value} onAdd={handleAdd} onLock={handleLock} onDelete={removeBranchSection} />)}
        </div>
      </div>  
    </>
  );
};

Branch.propTypes = {
  branch: PropTypes.object.isRequired,
  numberOfSlots: PropTypes.number.isRequired,
  onEdit: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired, 
  onDelete: PropTypes.func, 
};

export default Branch;