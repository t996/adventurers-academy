import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import 'bootstrap/dist/css/bootstrap.css';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';


const SectionInSlot = ({ slot, onDelete, onLock }) => {

  const renderTooltip = props => (

    <Tooltip className="section-title-tooltip" {...props}>{slot.title}</Tooltip>

  );


  return (
    <OverlayTrigger placement="top" overlay={renderTooltip}>
      <div className="slot-small-panel">
        <button className="academy-button close-button" style={{ paddingRight: '0.3rem' }} onClick={onDelete}>x</button>
        <div className="body">
          {slot.title}
        </div>
        <div className="footer">
          
          <div className="timelock-panel" onClick={onLock}>
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" className="bi bi-hourglass-split" viewBox="0 0 16 16">
              <path d="M2.5 15a.5.5 0 1 1 0-1h1v-1a4.5 4.5 0 0 1 2.557-4.06c.29-.139.443-.377.443-.59v-.7c0-.213-.154-.451-.443-.59A4.5 4.5 0 0 1 3.5 3V2h-1a.5.5 0 0 1 0-1h11a.5.5 0 0 1 0 1h-1v1a4.5 4.5 0 0 1-2.557 4.06c-.29.139-.443.377-.443.59v.7c0 .213.154.451.443.59A4.5 4.5 0 0 1 12.5 13v1h1a.5.5 0 0 1 0 1h-11zm2-13v1c0 .537.12 1.045.337 1.5h6.326c.216-.455.337-.963.337-1.5V2h-7zm3 6.35c0 .701-.478 1.236-1.011 1.492A3.5 3.5 0 0 0 4.5 13s.866-1.299 3-1.48V8.35zm1 0v3.17c2.134.181 3 1.48 3 1.48a3.5 3.5 0 0 0-1.989-3.158C8.978 9.586 8.5 9.052 8.5 8.351z"/>
            </svg>
            {slot.timeLock ? moment(slot.timeLock).format('YYYY-MM-DD') : ''}
          </div>
          {slot.isEmbedded ? 
            <div className="embedded-panel">
              {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" fill="currentColor" className="bi bi-file-code" viewBox="0 0 16 16">
                <path d="M6.646 5.646a.5.5 0 1 1 .708.708L5.707 8l1.647 1.646a.5.5 0 0 1-.708.708l-2-2a.5.5 0 0 1 0-.708l2-2zm2.708 0a.5.5 0 1 0-.708.708L10.293 8 8.646 9.646a.5.5 0 0 0 .708.708l2-2a.5.5 0 0 0 0-.708l-2-2z"/>
                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
              </svg> */}
          &lt;/&gt;&nbsp; embedded </div>: ''}          
          
        </div>
      </div>
    </OverlayTrigger>
  );
};

SectionInSlot.propTypes = {
  slot: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default SectionInSlot;