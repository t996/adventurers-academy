import './BranchesTabPanel.css';
import React, { useState } from 'react';
import { useCourseData } from '../../../hooks/courseContext';
import { createBranch, updateBranch } from '../../../services/http-branch-requests';
import Branch from './Branch';
import BranchForm from './BranchForm';
import { Modal } from 'react-bootstrap';


const getNumberOfSlots = (branches) => {
  let maxPosition = 0;
  branches.forEach(branch => {
    branch.sections.forEach(section => {
      if (maxPosition < section.position) {
        maxPosition = section.position;
      }
    });
  });

  if (maxPosition < 7) {
    maxPosition = 7; // 8 slots
  }

  // positions are zero based
  // we want at least 10 slots
  // and we want at least two empty slots after the last position
  return maxPosition + 1 + 2; 
};

const BranchesTabPanel = () => {
  const { courseData: course, dispatch } = useCourseData();
  const { branches } = course;

  const [selected, setSelected] = useState(null);
  const [modal, setModal] = useState(''); // modal: '', 'create', 'edit', 'delete'
  const [error, setError] = useState(null);
  
  const sectionSlots = getNumberOfSlots(branches);


  const handleCreate = () => {
    setModal('create');
    setSelected(null);
  };

  const handleEdit = (branch) => {
    setModal('edit');
    setSelected(branch);
  };

  const handleCancel = () => {
    setModal('');
    setSelected(null);
  };

  const create = async (form) => {    
    const data = {
      title: form.title,
      isPrivate: form.isPrivate,
      courseId: course.id,
      sections: [],
    };

    try {
      const result = await createBranch(data);
      
      if (result.error) {
        throw new Error(result.message);
      }

      dispatch({ type: 'createBranch', data: result });
      setSelected(null);
      setModal('');
    }
    catch(err) {
      setError(err);
    }
  };

  const update = async (form) => {
    const data = {
      id: form.id,
      title: form.title,
      isPrivate: form.isPrivate,
      sections: form.sections,   
    };

    try {
      const result = await updateBranch(data.id, data);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      dispatch({ type: 'updateBranch', data: result });
      setSelected(null);
      setModal('');      
    }
    catch(err) {
      setError(err);
    }
  };

  if (error) throw error;

  
  return (
    <>
      <Modal
        show={modal !== ''}
        onHide={handleCancel}
        size="mg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        animation={false}
        backdrop="static"
        keyboard={false}
        dialogClassName="branch-form-modal-container"
      >      
        <Modal.Body>
          {modal === 'create' && <BranchForm branch={null} onCancel={handleCancel} onSubmit={create} />}
          {modal === 'edit' && <BranchForm branch={selected} onCancel={handleCancel} onSubmit={update} />}
          {modal === 'delete' && 'delete'}
        </Modal.Body>
      </Modal>      
    
      <div className="react-tabs-content">
        <div className="react-tabs-inner-content">
          <div className='action-button-container'>
            <button className="academy-button" onClick={handleCreate} >Create Branch</button>
          </div>

          <div className="branch-list-container">
            {branches.map((b) => <Branch key={b.id} numberOfSlots={sectionSlots} branch={b} onEdit={() => handleEdit(b)} onUpdate={update} />)}
          </div>
        </div>
      </div> 
    </>
  );
};

export default BranchesTabPanel;