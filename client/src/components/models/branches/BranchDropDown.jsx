import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'react-bootstrap';


const BranchDropDown = ({ onEdit, onDelete }) => {

  const CustomToggle = React.forwardRef(({ children, style, onClick }, ref) => (
    <div
      ref={ref}
      style={style}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      <div style={{fontSize: '1rem', cursor: 'pointer' }}>
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-gear-fill" viewBox="0 0 16 16">
          <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
        </svg>
      </div>
    </div>
  ));
  

  const CustomMenu = React.forwardRef(
    ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
  
      return (
        <div
          ref={ref}
          style={style}
          className={className}
          aria-labelledby={labeledBy}
        >
          <ul className="list-unstyled">
            {children}
          </ul>
        </div>
      );
    },
  );
  
  return(
    <Dropdown align="right">
      <Dropdown.Toggle as={CustomToggle} />
      <Dropdown.Menu as={CustomMenu} align="right" className="header-dropdown-menu" >
        <Dropdown.Item as="button" onClick={onEdit}>Edit</Dropdown.Item>
        {/* <Dropdown.Item as="button" onClick={onDelete}>Delete</Dropdown.Item> */}
      </Dropdown.Menu>
    </Dropdown>
  );
};

BranchDropDown.propTypes = {
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func,
};

export default BranchDropDown;