import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';


const TimeLockForm = ({ branchSection, onSubmit, onCancel }) => {  
  
  const { register, handleSubmit } = useForm();
  
  const localSubmit = (form) => {    
    onSubmit({ ...form, timeLock: form.timeLock ? form.timeLock : null });    
  };

    
  return (     
    <form className="timelock-form" onSubmit={handleSubmit(localSubmit)}>
      
      <input 
        type="date"
        name="timelock" 
        placeholder=""
        className="academy-field" 
        defaultValue={branchSection.timeLock ? moment(branchSection.timeLock).format('YYYY-MM-DD') : ''} 
        {...register('timeLock')}
      />
      <input 
        type="hidden" 
        name="sectionId" 
        defaultValue={branchSection.section} 
        {...register('sectionId')}
      />
    
      
      {/* {isFormValid ? null : <div className="error-message">{validationMsg}</div>} */}

      <div style={{ marginTop: '1.5rem'}}>
        <button type="submit"  className="academy-button">
          Lock
        </button>
        
        <button type="button" onClick={onCancel}  className="academy-button">
          Cancel
        </button>
      </div>      
    </form>
  );
};

TimeLockForm.propTypes = {
  branchSection: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,  
};

export default TimeLockForm;