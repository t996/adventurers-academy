import React from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';


const BranchForm = ({ branch, onSubmit, onCancel }) => {  
  
  const { register, handleSubmit, formState: { errors } } = useForm();
  
  const localSubmit = (form) => {
    if (branch) {
      onSubmit({ ...form, id: branch.id, sections: branch.sections });
    }
    else {
      onSubmit({ ...form, sections: [] });
    }

  };

  
  return (     
    <form className="branch-form" onSubmit={handleSubmit(localSubmit)}>
      <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', textAlign: 'left'}}>
        <input 
          type="text"
          name="title"
          className="academy-field" 
          placeholder="Branch Title" 
          defaultValue={branch ? branch.title : ''} 
          {...register('title', { 
            required: { value: true, message: 'Title is required' }, 
            minLength: { value: 2, message: 'Title must be at least 2 chars.'},
            maxLength: { value: 30, message: 'Title must be less than 30 chars.'}, })
          } 
        />
      
        
        <label htmlFor="isPrivate" className="academy-field" style={{marginLeft: '0.25rem' }}>
          <input 
            type="checkbox"
            name="isPrivate"
            id="isPrivate"
            style={{ marginRight: '0.2rem' }}
            defaultChecked={ branch ? branch.isPrivate : false}
            {...register('isPrivate', { 
              required: false})
            }
          />
          Private
        </label>
      
      </div>
      
      {errors.title ? <div className="error-message">{errors.title.message}</div> : <div className="error-message">&nbsp;</div>}

      <div style={{ marginTop: '1.5rem'}}>
        <button type="submit" className="academy-button">
          {branch ? 'Update' : 'Create'}
        </button>
        
        <button type="button" onClick={onCancel} className="academy-button">
          Cancel
        </button>
      </div>
    </form>
  );
};

BranchForm.propTypes = {
  branch: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default BranchForm;
