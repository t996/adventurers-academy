import React, { useState } from 'react';
import { getAllUsers } from '../../../services/http-user-services';


const SearchUsers = () => {

  const [searchTerm, setSearchTerm] = useState('');
  const [resultSearch, setResultSearch] = useState(null);

  
  const handleChangeTerm = (e) => {
    setSearchTerm(e.target.value);    
  };


  const searchUser = (e) => {
    e.preventDefault();

    getAllUsers(searchTerm)
      .then(data => {
        setSearchTerm('');
        if (data.data.length !== 0) {
          setResultSearch(data.data);
        } else {
          return alert('No matches found!');
        }
      })
      .catch(err => {
        return alert(`${err.message}`);
      });
  };

  
  return (
    <>
      <form onSubmit={searchUser}>
        <input
          placeholder='Search user'
          type='text'
          onChange={handleChangeTerm}
          value={searchTerm}
        />
        {!searchTerm
          ? <button type="submit" disabled={true}>Search</button>
          : <button type="submit">Search</button>
        }   
      </form>
      {resultSearch
        ? resultSearch.map((user) => {
          return (
            <div key={user.id}>{user.email}</div>
          );
        })
        : null
      }
    </>
  );

};

export default SearchUsers;