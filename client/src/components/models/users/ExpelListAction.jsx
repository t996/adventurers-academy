import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useCourseData } from '../../../hooks/courseContext';
import { removeUserFromCourse } from '../../../services/http-course-requests';
// import SpinnerSmall from '../../general/SpinnerSmall';


const ExpelListAction = ({ user }) => {
  const { courseData: course, dispatch } = useCourseData();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleClick = async () => {
    setLoading(true);
    
    try {
      const result = await removeUserFromCourse(course.id, user.id);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      dispatch({ type: 'expelUser', user });      
      setLoading(false);
    }
    catch(err) {
      setError(err);
      setLoading(false);
    }
   
  };
  
  if (error) throw error;
  // if (loading) return <SpinnerSmall />;

  return (
    <div className="list-action">
      <div className="action">
        <button  className="academy-button"  disabled={loading} onClick={handleClick}>expel</button>
      </div>      
    </div>
  );

};


ExpelListAction.propTypes = {
  user: PropTypes.object.isRequired,   
};

export default ExpelListAction;