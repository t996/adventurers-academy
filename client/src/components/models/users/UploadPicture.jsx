import React, {useState} from 'react';
import PropTypes from  'prop-types';
import { BASE_URL } from '../../../common/constants';
import { removePicture, updatePicture } from '../../../services/http-user-services';


const placeholderImageSrc = process.env.PUBLIC_URL + 'images/portrait-placeholder.jpg';
const getProfileSrc = (profile) => {
  if (profile && profile.picture) {
    return `${BASE_URL}/users/picture/${profile.picture}`;
  }
  return placeholderImageSrc;
};


const UploadPicture = ({ profile }) => {
  const [preview, setPreview] = useState(getProfileSrc(profile));

  const changeHandler = (event) => {
    
    if (event.target.files && event.target.files[0]) {

      const file = event.target.files[0];
   
      if ((file.type === 'image/gif' 
      || file.type === 'image/jpeg' 
      || file.type === 'image/png')
      && file.size > 0 && file.size < 300 * 1024) {
        
        const formData = new FormData();
        formData.append('profilePicture', file);
    
        updatePicture(formData)
          .then((result) => {
            setPreview(URL.createObjectURL(file));
          })
          .catch((error) => {
            console.error('Error:', error);
          });
      }
    }    
  };

  const handleClear = () => {
    removePicture()
      .then((result) => {
        setPreview(placeholderImageSrc);
      })
      .catch((error) => {
        console.error('Error:', error);
      });    
  };

  return(
    <div className="portrait-holder">
      <img src={preview} alt='' className="profilePic"/>
      <div className="portrait-image"> 
        <button className="academy-button close-button" onClick={handleClear}>x</button>
      </div>
      
      <label className="academy-button" htmlFor="profilePicture" >
        <input id="profilePicture" name="profilePicture" type="file" className="d-none" accept=".gif,.jpg,.jpeg,.png" multiple={false} onChange={changeHandler}/>
        Select Picture
      </label>
    </div>
  );
};

UploadPicture.propTypes = {
  profile: PropTypes.object.isRequired,
};


export default UploadPicture;