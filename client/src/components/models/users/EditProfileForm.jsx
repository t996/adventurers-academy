import { useState } from 'react';
import PropTypes from  'prop-types';

const EditProfileForm = ({profile, onSubmitFunc, onCancelFunc}) => {

  const [form, setForm] = useState({
    firstName: {
      name: 'firstName',
      value: profile.firstName,
      type: 'text',
    },
    lastName: {
      name: 'lastName',
      value: profile.lastName,
      type: 'text',
    },
  });


  const setChange = (event) => {
    const {name, value} = event.target;
    form[name].value = value;
    setForm({...form});
  };


  return (
    <form onSubmit={e => onSubmitFunc(e, form.firstName.value, form.lastName.value)} id='editForm'>
      <label htmlFor="first-name" style={{padding: '0px 5px 0px 0px'}}> First name &nbsp;
        <input 
          className="academy-field"
          id="first-name"
          type={form.firstName.type}
          name={form.firstName.name}
          value = {form.firstName.value}
          onChange={setChange}/> <br/>
      </label><br></br>
              
      <label htmlFor="last-name"> Last name &nbsp;
        <input 
          id="last-name"
          className="academy-field"
          type = {form.lastName.type}
          name = {form.lastName.name}
          value = {form.lastName.value}
          onChange={setChange}/> <br/>
      </label><br></br>

      <input type='submit' value='Submit' className="academy-button"></input>&nbsp; 
      <button className="academy-button" onClick={e => onCancelFunc()}>Cancel</button>
    </form> 
  );
};

EditProfileForm.propTypes = {
  profile: PropTypes.object.isRequired, 
  onSubmitFunc: PropTypes.func.isRequired,
  onCancelFunc: PropTypes.func.isRequired,  
};


export default EditProfileForm;