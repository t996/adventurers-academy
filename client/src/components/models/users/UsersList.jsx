import React from 'react';
import PropTypes from 'prop-types';
import { BASE_URL } from '../../../common/constants';


const placeholderImageSrc = process.env.PUBLIC_URL + 'images/portrait-placeholder.jpg';
const getProfileSrc = (user) => {
  if (user && user.picture) {
    return `${BASE_URL}/users/picture/${user.picture}`;
  }
  return placeholderImageSrc;
};


const UsersList = ({ data: users, ListAction, listActionProps }) => {

  if (users.length === 0) {
    return (
      <div style={{ paddingTop: '2rem' }}></div>
    );
  }
  
  const transformedUsers = users.map((user) => {
    return (
      <li key={user.id} className="user-list-item">
      
        <div className="title">
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <div className="portrait">
              <img src={getProfileSrc(user)} alt='' className="user-list-portrait"/>
            </div>
          
            {`${user.firstName} ${user.lastName}`}
          </div>
         
          <ListAction user={user} {...listActionProps} />
        </div>

        {/* <div className="status" >
          ({user.guildClass}) ({user.guildRank}) {user.email}
        </div> */}
      </li>);
  });
  
  return (
    <ul className="user-list">
      {transformedUsers}
    </ul>
  ); 
};


UsersList.propTypes = {
  data: PropTypes.array.isRequired,
  ListAction: PropTypes.func.isRequired,
  listActionProps: PropTypes.object.isRequired,
};

export default UsersList;
