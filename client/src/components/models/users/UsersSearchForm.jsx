import React from 'react';
import PropTypes from  'prop-types';
import { useForm } from 'react-hook-form';


const UsersSearchForm = ({ setFilter }) => {
  const { register, handleSubmit } = useForm();
  
  const onSubmit = (form) => {
    setFilter(form);
  };

   
  return (
    <form className="user-search-form" onSubmit={handleSubmit(onSubmit)} >
      <div className="main-controls">
        <input
          type="text"
          name='query'
          className="search-query academy-field"
          placeholder='Enter a term to search'
          {...register('query')} 
        />

        <button type="submit" className="academy-button">Search</button>
      </div>
    </form>
  );
};

UsersSearchForm.propTypes = {
  setFilter: PropTypes.func.isRequired,  
};

export default UsersSearchForm;
