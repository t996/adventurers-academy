import './UsersSearchForm.css';
import React, { useState } from 'react';
import { useCourseData } from '../../../hooks/courseContext';
import ListContainer from '../../general/ListContainer';
import UsersSearchForm from './UsersSearchForm';
import UsersList from './UsersList';
import { getUsers } from '../../../services/http-user-services.js';
import EnrollListAction from './EnrollListAction';
import CourseUsersSearchForm from './CourseUsersSearchForm';
import ExpelListAction from './ExpelListAction';
import AccessListAction from './AccessListAction';


const UsersTabPanel = () => {
  const { courseData: course } = useCourseData();
  const { users } = course;
  const [filter, setFilter] = useState(null);
  const [courseFilter, setCourseFilter] = useState(null);

  //const [error, setError] = useState(null);

 
  const getCourseUsers = async (fil) => {
    let filteredUsers = [...users];
    
    if (fil.query) {
      filteredUsers = filteredUsers.filter(user => {
        return user.email.toLowerCase().includes(fil.query.toLowerCase()) 
        || (user.firstName + ' ' + user.lastName).toLowerCase().includes(fil.query.toLowerCase());               
      });
    }
    
    if (fil.accessOnly && fil.branch !== 0) {
      filteredUsers = filteredUsers.filter(u => !!u.branches.find(b => b.id === fil.branch));
    } 
    
    return filteredUsers;
  };

  // if (error) throw error;

  const accessListProps = {};
  if (courseFilter && courseFilter.branch) {
    accessListProps.branchId = courseFilter.branch;
  }

  return (
    <div className="react-tabs-content"> 
      <div className="tab-division" style={{ paddingLeft: '2rem' }}>
        <div className="course-users">
          <div className="search">
            <CourseUsersSearchForm setFilter={setCourseFilter} />
          </div>
         
          <ListContainer 
            filter={courseFilter} 
            setFilter={setCourseFilter} 
            getData={getCourseUsers} 
            ListComponent={UsersList} 
            ListAction={ courseFilter && courseFilter.branch ? AccessListAction : ExpelListAction} 
            listActionProps={accessListProps} 
          />         
        </div>
      </div>
      <div className="tab-division" style={{ paddingRight: '2rem' }}>
        <div className="users">
          <div className="search">
            <UsersSearchForm filter={filter} setFilter={setFilter} />
          </div>
      
         
          <ListContainer 
            filter={filter} 
            setFilter={setFilter} 
            getData={getUsers} 
            ListComponent={UsersList} 
            ListAction={EnrollListAction} 
            listActionProps={{}} 
          />
         
        </div>  
      </div>
    </div>

  );
};

export default UsersTabPanel;