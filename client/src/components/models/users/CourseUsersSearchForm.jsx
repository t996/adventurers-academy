import './UsersSearchForm.css';
import React, { useEffect } from 'react';
import PropTypes from  'prop-types';
import { useForm } from 'react-hook-form';
import { useCourseData } from '../../../hooks/courseContext';


const CourseUsersSearchForm = ({ setFilter }) => {
  const { register, handleSubmit, watch } = useForm();
  const { courseData: course } = useCourseData();
  const { branches } = course;

  const query = watch('query', '');
  const branch = watch('branch', '0');
  const accessOnly = watch('accessOnly', false);

  
  useEffect(() => {
    const tempForm = {
      query,
      branch: +branch,
      accessOnly,
    };
    setFilter(tempForm);
  }, [query, branch, accessOnly, setFilter]);

  const onSubmit = (form) => {

    const modifiedForm = { 
      query: form.query,
      accessOnly: form.accessOnly,
    };
    
    if (form.branch !== '0') {
      modifiedForm.branch = +form.branch;
    }

    setFilter(modifiedForm);
  };

  return (
    <form className="user-search-form" onSubmit={handleSubmit(onSubmit)} >
      <div className="main-controls">
        <input
          type="text"
          name='query'
          className="search-query academy-field"
          placeholder='Enter a term to search'
          style={{ width: '30rem', }}
          {...register('query')} 
        />

        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start'}}>
          <select
            type="select"
            name="branch"
            className="search-branch academy-field"
            defaultValue=""
            {...register('branch')} >
            <option value="0">public</option>
            { branches.filter(b => b.isPrivate).map(b => {
              return (
                <option key={b.id} value={b.id}>{b.title}</option>
              );
            })}
          </select>
          <label className="form-check-label academy-field" style={{  marginLeft: '0.5rem' }}>
            <input
              className="form-check-input" 
              type="checkbox"
              defaultChecked={false}
              style={{ marginRight: '0.25rem'}}
              {...register('accessOnly')}
            />
            With access only
          </label>       
        </div>

        {/* <button type="submit">Search</button> */}
      </div>
    </form>
  );
};

CourseUsersSearchForm.propTypes = {
  setFilter: PropTypes.func.isRequired,  
};

export default CourseUsersSearchForm;
