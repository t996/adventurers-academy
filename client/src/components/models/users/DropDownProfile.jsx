import React, { useContext } from 'react';
import { Dropdown } from 'react-bootstrap';
import AuthContext from '../../../providers/AuthContext';
import { isTeacher, logoutUser } from '../../../services/auth-services';
import { useHistory } from 'react-router-dom';
import { useMyCoursesData } from '../../../hooks/myCoursesContext';

const MyProfile = () => {

  const {setLoggedUser} = useContext(AuthContext);
  const {dispatch} = useMyCoursesData();
  const history = useHistory();

  const CustomToggle = React.forwardRef(({ children, style, onClick }, ref) => (
    <div
      // href=""
      ref={ref}
      style={style}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      <div style={{fontSize: '1rem', cursor: 'pointer'/*, marginTop: '0.5rem', marginRight: '2rem'*/}} id='dropPic'>
        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" className="bi bi-list" viewBox="0 0 13 13">
          <path fillRule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
        </svg>
      </div>
    </div>
  ));
  

  const CustomMenu = React.forwardRef(
    ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
  
      return (
        <div
          ref={ref}
          style={style}
          className={className}
          aria-labelledby={labeledBy}
        >
          <ul className="list-unstyled">
            {children}
          </ul>
        </div>
      );
    },
  );
  
  return(
    <Dropdown align="right">
      <Dropdown.Toggle as={CustomToggle} />
      <Dropdown.Menu as={CustomMenu} align="right" className="header-dropdown-menu" >
        <Dropdown.Item as="button" onClick={e => history.push('/profile')}>Profile</Dropdown.Item>
        
        {isTeacher() 
          ? <>
            <Dropdown.Item as="button" onClick={e => history.push('/home')}>Courses</Dropdown.Item>
            <Dropdown.Item as="button" onClick={e => history.push('/courses/create')}>New Course</Dropdown.Item>            
          </>
          : <Dropdown.Item as="button" onClick={e => history.push('/home')}>My Courses</Dropdown.Item>
        }
        <Dropdown.Item as="button" onClick={e => logoutUser(setLoggedUser, dispatch)}>Logout</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default MyProfile;