import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useCourseData } from '../../../hooks/courseContext';
import { addUserToCourse } from '../../../services/http-course-requests';
// import SpinnerSmall from '../../general/SpinnerSmall';


const EnrollListAction = ({ user }) => {
  const { courseData: course, dispatch } = useCourseData();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleClick = async () => {
    setLoading(true);
    
    try {
      const result = await addUserToCourse(course.id, user.id);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      dispatch({ type: 'enrollUser', user });      
      setLoading(false);
    }
    catch(err) {
      setError(err);
      setLoading(false);
    }
  };
  
  const isEnrolled = !!course.users.find(u => u.id === user.id);

  if (error) throw error;
  // if (loading) return <SpinnerSmall />;


  return (
    <div className="list-action">
      {isEnrolled
        ? <div className="action-status" style={{marginRight: '0.5rem' }}>enrolled</div>
        : <div className="action">
          <button className="academy-button" disabled={loading} onClick={handleClick}>enroll</button>
        </div>
      }
    </div>
  );

};


EnrollListAction.propTypes = {
  user: PropTypes.object.isRequired,  
};

export default EnrollListAction;