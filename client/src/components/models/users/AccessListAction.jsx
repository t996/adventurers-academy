import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useCourseData } from '../../../hooks/courseContext';
// import SpinnerSmall from '../../general/SpinnerSmall';
import { addAccessToBranch, removeAccessToBranch } from '../../../services/http-branch-requests';


const AccessListAction = ({ user, branchId }) => {
  const { dispatch } = useCourseData();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);


  const addAccess = async () => {
    setLoading(true);
    
    try {
      const result = await addAccessToBranch(user.id, branchId);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      dispatch({ type: 'addBranchToUser', user, branchId });      
      setLoading(false);
    }
    catch(err) {
      setError(err);
      setLoading(false);
    }
  };
  
  const removeAccess = async () => {
    setLoading(true);
    
    try {
      const result = await removeAccessToBranch(user.id, branchId);
      
      if (result.error) {
        throw new Error(result.message);
      }
      
      dispatch({ type: 'removeBranchToUser', user, branchId });      
      setLoading(false);
    }
    catch(err) {
      setError(err);
      setLoading(false);
    }
  };

  const hasAccessToBranch = user.branches.find(b => b.id === branchId);

  if (error) throw error;
  // if (loading) return <SpinnerSmall />;

  return (
    <div className="list-action">
      {hasAccessToBranch
        ? <div className="action">
          <button className="academy-button" disabled={loading} onClick={removeAccess}>revoke access</button>          
        </div>
        : <div className="action">
          <button className="academy-button" disabled={loading} onClick={addAccess}>give access</button>
        </div>
      }
    </div>
  );

};


AccessListAction.propTypes = {
  user: PropTypes.object.isRequired, 
  branchId: PropTypes.number.isRequired,
};

export default AccessListAction;