import React from 'react';
import Header from './Header';
import Footer from './Footer';


const Layout = (props) => {
  return (
    <>
      <Header {...props} />
      <div className='main'>     
        {props.children}      
      </div>
      <Footer {...props} />
    </>
  );
};

export default Layout;