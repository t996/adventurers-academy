import { useContext } from 'react';
// import { NavLink } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import DropDownProfile from '../models/users/DropDownProfile';
import { useHistory } from 'react-router-dom';


const Header = () => {

  const history = useHistory();
  const {user} = useContext(AuthContext);

  return (
    <div id='header'>
      <div className='row mt-2 intro' > 
        <div className='col-lg-2'></div>
        <div className='col-lg-8 logo' onClick={e=> history.push('/home')}>Adventurers' Academy</div>
        <div className='col-lg-2 signIn'>      
          {!user
            ? (
              <button className="academy-button" onClick={e => history.push('/login')}> Sign in
                {/* <NavLink className='sign-nav' to="/login">Sign in</NavLink> */}
              </button>
            )
            : <DropDownProfile id='dropdown' />    
          }
        </div> 
      </div> 
      {/* <div className='row'>
        <div className='col-2'></div>
        <div className='col-8 mt-2 slogan'>Your journey starts here!</div>
        <div className='col-2 col-md-1'></div>
      </div >   */}
    </div>
  );
};

export default Header;
