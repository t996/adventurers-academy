import React from 'react';
import { withRouter } from 'react-router-dom';
import { removeToken } from '../../services/auth-services';


class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {    
    
    // don't show fallback UI in case of redirect to prevent flickering
    // if (error.message.toLowerCase() === 'unauthorized' 
    //      || error.message.toLowerCase() === 'forbidden'
    //      || error.message.toLowerCase() === 'not found') {
    //   return { hasError : false };
    // }

    // Update state so the next render will show the fallback UI. 
    return { hasError: true };  
  }
  componentDidCatch(error, errorInfo) {    
    // You can also log the error to an error reporting service    
    // logErrorToMyService(error, errorInfo); 
    
    
    if (error.message.toLowerCase() === 'unauthorized' || error.message.toLowerCase() === 'error: unauthorized') {
      removeToken(); // remove the token just in case there is a stale one
      this.props.history.push('/login');
      // window.location.reload();
    }

    if (error.message.toLowerCase() === 'forbidden' || error.message.toLowerCase() === 'error: forbidden') {
      this.props.history.push('/forbidden');
      // window.location.reload();
    }

    if (error.message.toLowerCase() === 'not found' || error.message.toLowerCase() === 'error: not found') {
      this.props.history.push('/notfound');
      // window.location.reload();
    }

    // This will only change the url in the browser, depending on what else you have your ErrorBoundary doing you need to call
    
  }
  render() {
    if (this.state.hasError) {      
      // You can render any custom fallback UI      
      return <h1>Something went wrong.</h1>;    
    }
    return this.props.children; 
  }
}

export default withRouter(ErrorBoundary);
