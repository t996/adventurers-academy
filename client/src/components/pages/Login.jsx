import { useContext, useState } from 'react';
import { getUserFromToken } from '../../services/auth-services';
import { userLogin } from '../../services/http-user-services';
import AuthContext from '../../providers/AuthContext';
import * as EmailValidator from 'email-validator';
import { NavLink } from 'react-router-dom';

const Login = (props) => {

  const {setLoggedUser} = useContext(AuthContext);

  const [user, setUser] = useState({
    email: '',
    password: '',
  });
  const [error, setError] = useState(null);
  const [isFormValid, setFormValid] = useState(true);

  const validations = {
    email: (value) => EmailValidator.validate(value),
    password: (value) => typeof value === 'string' && value.length > 7 && value.length < 45,
  };


  const setInputField = (key, value) => {
    user[key] = value;
    setUser({...user});
    setFormValid(validations[key](value));
  };


  const triggerLogin = (e) => {
    e.preventDefault();
    
    userLogin(user)
      .then(data => {
        setLoggedUser(getUserFromToken(data));
        props.history.push('/home');
      })
      .catch(err => {
        setError(err.message);
      });
  };
  
  return (
    <div className='row mt-5 login'>
      <div className='col-12 mt-5'>
        <form onSubmit={triggerLogin}>
          <div>
            <input required type='text' size='40'
              placeholder='email' 
              onChange={e => setInputField('email', e.target.value)}
              className={'academy-field' + (isFormValid ? ' valid' : ' invalid')}
              style={{marginBottom: '0.5rem'}}
            />
          </div>
          <div>
            <input required type='password' size='40'
              placeholder='password' 
              onChange={e => setInputField('password', e.target.value)}
              className={'academy-field' + (isFormValid ? ' valid' : ' invalid')}
              style={{marginBottom: '0.5rem'}}
            /> 
          </div>
          <div className='error-msg'>
            {error
              ? <div style={{color: 'red'}}>{error}. Try again!</div>
              : null
            }
          </div>
          <div className="mt-4">
            <input type='submit' value='Login' className="academy-button" disabled={!isFormValid}></input>
            <div style={{fontWeight: 'bolder'}}>Don't have an account?</div> 
            <NavLink to="/register">Register</NavLink>
          </div>
        </form>
      </div>
    </div>
  );

};

export default Login;