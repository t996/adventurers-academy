import React from 'react';
import './AboutUs.css';

const AboutUs = () => {

  return (
    <div className='container'>
      <div className='authors' >  
        <div className='singleAuthor'>
          <h5>Radoslav Hadzhiev</h5>
          <div className='picture'
            style={{
              backgroundImage: `url(${process.env.PUBLIC_URL + 'ejko2.jpg'})`, 
            }}>        
          </div>
        </div>
        <div className='singleAuthor'>
          <h5>Nikoleta Minkova</h5>
          <div className='picture'
            style={{
              backgroundImage: `url(${process.env.PUBLIC_URL + 'zayo.png'})`, 
            }}>
          </div>
        </div>
        
      </div>
      <span className='intro'> Started a month ago and finished few hours ago... <br></br>
      Motivated by the phrase "A problem shared is a problem halved"...<br></br>
      We did everything best to create one useful forum for those who are addicted to programming.<br></br>
      Register and enjoy!
      </span>
    </div>
  );
};


export default AboutUs;