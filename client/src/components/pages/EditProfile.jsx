import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/AuthContext';
import { getUserProfile, updateUserProfile } from '../../services/http-user-services';
import Spinner from '../general/Spinner';
import EditProfileForm from '../models/users/EditProfileForm';
import UploadPicture from '../models/users/UploadPicture';


const EditProfile = (props) => {

  const {user} = useContext(AuthContext);
  
  const [profile, setProfile] = useState(null);
  const [editable, setEditable] = useState(false);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  
  useEffect(() => {
    
    setLoading(true);
    
    getUserProfile()
      .then(data => {
        setProfile(data);
        
        
        setLoading(false);
      })
      .catch(err => {
        //return alert(`${err.message}.`);
        setError(err);
        setLoading(true);
      });
  }, [user]);


  const changeProfile = (e, firstName, lastName) => {
    e.preventDefault();

    const body = {
      firstName, 
      lastName,
    };
    
    updateUserProfile(body)
      .then(data => {
        setProfile(data);
        setEditable(false);
        setError(null);
      })
      .catch(err => {
        //TODO: Test error for duplicate names
        setError(err);
      });
  };


  const closeEditForm = () => {
    setEditable(false);
  };

  if (error) throw new Error(error);
  if (loading) return <Spinner />;

 
  return (
    <div className='row mt-5 edit'>
      <div className='col-12 mt-2 editCol'>
        {profile
          ? <>
            <UploadPicture profile={profile} />
             
            <div className="not-edit">
              <div>Rank: {profile.guildRank} </div>
              <div>Class: {profile.guildClass} </div>
              <div>Email: {profile.email}</div>
            </div>

            {editable
              ? <EditProfileForm profile={profile} onSubmitFunc={changeProfile} onCancelFunc={closeEditForm}/>
              : <>
                <div>First name: {profile.firstName}</div> 
                <div>Last name: {profile.lastName}</div>
                <button onClick={e => setEditable(true)} className="academy-button">Edit</button>
              </>
            }
            {error
              ? <h4 style={{color: 'red'}}>
                <div>{error}. Try again!</div>                    
              </h4>
              : null
            }            
          </>
          :null}
      </div>
    </div>
  );
};

export default EditProfile;