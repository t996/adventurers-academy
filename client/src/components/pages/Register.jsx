import { useContext, useState } from 'react';
import { registerUser } from '../../services/http-user-services';
import * as EmailValidator from 'email-validator';
import AuthContext from '../../providers/AuthContext';
import { getUserFromToken } from '../../services/auth-services';

const Register = (props) => {

  const { setLoggedUser } = useContext(AuthContext);

  const [form, setForm] = useState({
    email: {
      name: 'email',
      placeholder: 'email',
      value: '',
      type: 'text',
      valid: true,
      touched: false,
      required: true,
    },
    password: {
      name: 'password',
      placeholder: 'password',
      value: '',
      type: 'password',
      valid: true,
      touched: false,
      required: true,
    },
    secondPassword: {
      name: 'secondPassword',
      placeholder: 'repeat password',
      value: '',
      type: 'password',
      valid: true,
      touched: false,
      required: true,
    },
  });

  const [formValid, setFormValid] = useState(true);
  const [error, setError] = useState(null);

  const validations = {
    email: (value) => EmailValidator.validate(value),
    password: (value) => typeof value === 'string' && value.length > 7 && value.length < 45,
    secondPassword: (value) => typeof value === 'string' && value.length > 7 && value.length < 45,
  };


  const handleInputChange = (event) => {

    const {name, value} = event.target;
    form[name].value = value;
    form[name].valid = validations[name](value);
    form[name].touched = true;
    setForm({...form});
    
    setFormValid(Object.values(form).every(el => el.valid && el.touched));
  };


  const register = (event) => {
    event.preventDefault();

    if(form.password.value !== form.secondPassword.value) {
      setError('Passwords must match!');
      return;
    };

    registerUser({
      email: form.email.value,
      password: form.password.value,
    })
      .then(data => {
        setLoggedUser(getUserFromToken(data));
        props.history.push('/profile');
      })
      .catch(err => {
        if(err.message === 'Conflict') {
          setError('Such user already exists');
        } else {
          setError(err.message);
        }
      });
  };


  const inputFields = Object.entries(form).map(([key, el]) => {
    return (
      <div key={key}>
        <input
          name={el.name}
          size='40'
          type={el.type}
          placeholder={el.placeholder}
          required={el.required}
          value={el.value}
          className={'academy-field' + (el.valid ? ' valid' : ' invalid')}
          style={{marginBottom: '0.5rem'}}
          onChange={handleInputChange}/> 
      </div>
    );
  });


  return (
    <div className='row mt-5 register'> 
      <form className='col-12 mt-5' onSubmit={register}>
        {inputFields}
        <div className="error-msg">
          {error
            ? <div style={{color: 'red'}}>{error}. Try again!</div>
            : null
          }
        </div>

        <button type='submit' className="academy-button mt-4" disabled={!formValid}>Register</button>
      </form>
    </div>
  );
}; 

export default Register;