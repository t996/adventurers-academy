import React, {useContext, useEffect, useState } from 'react';
import { getAllCourses, getAllCoursesPublic } from '../../services/http-course-requests';
import Spinner from '../general/Spinner';
import { Modal } from 'react-bootstrap';
import AuthContext from '../../providers/AuthContext';
import { useHistory } from 'react-router-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { isStudent, isTeacher} from '../../services/auth-services';
import { useMyCoursesData } from '../../hooks/myCoursesContext';
import CourseDescription from '../models/courses/CourseDescription';
import MyCourses from '../models/courses/MyCourses';
import CourseList from '../models/courses/CourseList';


const Home = (props) => {
  const { user } = useContext(AuthContext);

  const [courses, setCourses] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setIsLoading] = useState(true);   
  const [filter] = useState(null);

  const [visibleModal, setVisibleModal] = useState(false);
  const [toggledCourse, setToggledCourse] = useState(null);

  const {courseData: loggedUserCourses} = useMyCoursesData();

  const history = useHistory();


  const handleClick = (course) => {
    const isEnrolled = loggedUserCourses && !!loggedUserCourses.find( el => el.id === course.id);
    
    if (isEnrolled) {
      history.push(`courses/${course.id}`);
    } else {
      setToggledCourse(course);
      setVisibleModal(true);
    };
  };

  const hideModal = () => {
    setVisibleModal(false);
    setToggledCourse(null);
  };


  useEffect(() => {
    let unmounted = false;
    setIsLoading(true);
    setError(null);

    const getData = async () => {
      try {
        const result = user && isTeacher() ? await getAllCourses(filter) : await getAllCoursesPublic(filter);
          
        if (result.error) {
          throw new Error(result.message);
        }
          
        if (!unmounted) {
          setCourses(result);
          setIsLoading(false);
        }        
      }
      catch(err) {
        if (!unmounted) {
          setError(err);
          setIsLoading(false);
        }
      }
    };
    getData();
    return () => { unmounted = true; };
  }, [user, filter]);


  const showMyCourses = user && isStudent();

  if (error) throw error;
  if (loading) return <Spinner />;


  return (
    <>
      <Modal
        show={visibleModal}
        onHide={hideModal}
        size="md"
        animation={false}
        centered
      > 
        {visibleModal
          ? (
            <Modal.Body>
              <CourseDescription course={toggledCourse} hideActions={false} />
            </Modal.Body>
          )
          :null
        }
      </Modal>

      <Tabs defaultIndex={showMyCourses ? 1 : 0}>
        <TabList>
          <Tab>Courses</Tab>
          { user && !isTeacher() ? <Tab>My Courses</Tab> : null }          
        </TabList>      

        <TabPanel>
          <CourseList courses={courses} onClick={handleClick} />     
        </TabPanel>
        
        { user && !isTeacher() 
          ? <TabPanel>
            <MyCourses />
          </TabPanel>
          : null 
        }
      </Tabs>      
    </>
  ); 
};
export default Home;
   
