import React, { useEffect, useState } from 'react';
import Spinner from '../general/Spinner';
import { getCourseSections, quitCourse } from '../../services/http-course-requests';
import SectionView from '../models/sections/SectionView';
import { isTeacher } from '../../services/auth-services';
import { Breadcrumb, Modal } from 'react-bootstrap';
import { useMyCoursesData } from '../../hooks/myCoursesContext';
import { Link } from 'react-router-dom';


const CoursePage = ({ match, history }) => {

  const courseId = +match.params.id;

  const { courseData: myCourses } = useMyCoursesData();

  const course = myCourses ? myCourses.find(mc => mc.id === courseId) : undefined;

  const [sections, setSections] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setIsLoading] = useState(true);  

  const [visibleModal, setVisibleModal] = useState(false);
  const { dispatch } = useMyCoursesData();
  
  useEffect(() => {
    let unmounted = false;

    setIsLoading(true);
    setError(null);

    const getData = async () => {

      try {
        const result = await getCourseSections(courseId);
        
        if (result.error) {
          throw new Error(result.message);
        }
        
        if (!unmounted) {
          setSections(result);
          setIsLoading(false);
        }        
      }
      catch(err) {
        if (!unmounted) {
          setError(err);
          setIsLoading(false);
        }
      }
    };

    getData();
    return () => { unmounted = true; };
  }, [courseId]);


  const handleManage = () => {
    history.push(`/courses/${courseId}/manage`);
  };


  const handleQuit = () => {
   
    quitCourse(courseId)
      .then(data =>  {
        dispatch({ type: 'init', data: data });
        history.push('/home');
      })
      .catch(err => setError(err.message));
  };

  const hideModal = () => {
    setVisibleModal(false);
  };

  if (error) throw error;
  if (loading) return <Spinner />;

  return (
    <>
      <Modal
        show={visibleModal}
        onHide={hideModal}
        size="md"
        animation={false}
        centered
      > 
        {visibleModal
          ? (
            <Modal.Body>
              <div className="unsubscribe">
                <div className="mt-5 unsubscribe-msg">Are you sure that you want to quit this course?</div>
                <span className="unsubscribe-buttons mt-4"> 
                  <button className="academy-button confirm" onClick={handleQuit}>Confirm</button>
                  <button className="academy-button" onClick={e => setVisibleModal(false)}>Cancel</button>
                </span>
              </div>
            </Modal.Body>
          )
          :null
        }
      </Modal>

      <div className="action-panel">
        <button className="academy-button back" onClick={e => history.push('/home')}>Back</button>
        <Breadcrumb>
          <Breadcrumb.Item linkProps={{ to: '/home' }} linkAs={Link}> My Courses </Breadcrumb.Item>
          <Breadcrumb.Item active> {course && course.title} </Breadcrumb.Item>
        </Breadcrumb>
        {isTeacher() 
          ? <button onClick={handleManage} className="academy-button">Manage</button> 
          : <button onClick={e => setVisibleModal(true)} className="academy-button">Quit this course</button>}
      </div>
      <div className="course-view">
        <div className="course-sections-view" >
          <div className="course-view-title">
            <div className="section-top-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-01.png'})`}} ></div>
            <div className="course-title">
              {course ? <h2>{course.title}</h2> : ''}
            </div>
            {course && <div className="section-bottom-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-02.png'})`}} ></div>}
          </div>
          {sections.map(bs => <SectionView key={bs.section.id} branchSection={bs} courseId={courseId} disableLinks={false} ignoreDates={false} />)}
        </div>
      </div>
    </>
  );
};

export default CoursePage;