import React, { useState } from 'react';
import CourseForm from '../models/courses/CourseForm';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { createCourse } from '../../services/http-course-requests.js';


const CreateCourse = (props) => {  
  
  const [error, setError] = useState(null);


  const create = async (courseForm) => {
    const data = {
      title: courseForm.title,
      description: courseForm.description,
      isPrivate: courseForm.isPrivate,
    };

    try {

      const result = await createCourse(data);
      
      if (result.error) {
        throw new Error(result.message);
      }

      props.history.push(`/courses/${result.id}/manage`);
    }
    catch(err) {
      setError(err);
    }
  };
  
  if (error) throw error;
  
  return (    
    <Tabs>
      <TabList>
        <Tab>Create new course</Tab>
      </TabList>

      <TabPanel>
        <div className="react-tabs-content">
          <CourseForm course={null} onSubmit={create} onCancel={() => props.history.push('/home')} />
        </div>
      </TabPanel>
    </Tabs>    
  );
};

export default CreateCourse;
