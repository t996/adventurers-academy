import React, { useEffect, useState } from 'react';
import Spinner from '../general/Spinner';
import { getSection } from '../../services/http-section-requests';
import SectionContentView from '../models/sections/SectionContentView';
import { Breadcrumb } from 'react-bootstrap';
import { Link } from 'react-router-dom';


const SectionPage = ({ match, history }) => {

  const sectionId = +match.params.sid;
  const [section, setSection] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setIsLoading] = useState(true);   

  useEffect(() => {
    let unmounted = false;

    setIsLoading(true);
    setError(null);

    const getData = async () => {

      try {
        const result = await getSection(sectionId);
        
        if (result.error) {
          throw new Error(result.message);
        }
        
        if (!unmounted) {
          setSection(result);
          setIsLoading(false);
        }        
      }
      catch(err) {
        if (!unmounted) {
          setError(err);
          setIsLoading(false);
        }
      }
    };

    getData();
    return () => { unmounted = true; };
  }, [sectionId]);

  if (error) throw error;
  if (loading) return <Spinner />;

  return (
    <>
      <div className="action-panel">
        <button className="academy-button" onClick={e => history.goBack()}>Back</button>
        <Breadcrumb>
          <Breadcrumb.Item linkProps={{ to: '/home' }} linkAs={Link}> My Courses </Breadcrumb.Item>
          {section.length !== 0 
            ? <Breadcrumb.Item linkProps={{ to: `/courses/${section.course.id}` }} linkAs={Link}> {section.course.title} </Breadcrumb.Item>
            : null
          }
          <Breadcrumb.Item active> {section.title} </Breadcrumb.Item>
        </Breadcrumb>
        <div style={{visibility: 'hidden'}}></div>
      </div>

      <div className="course-view">
        <div className="course-sections-view">
          <div className="section-view" >
            <div className="section-top-divider" style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/images/dividers/divider-01.png'})`}} ></div>
            <SectionContentView section={section}/>
          </div>
        </div>
      </div>
    </>
  );
};

export default SectionPage;