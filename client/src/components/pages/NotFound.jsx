import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';


const NotFound = () => {
  return (
    <Tabs>
      <TabList>
        <Tab>Error</Tab>      
      </TabList>      

      <TabPanel>
        <h2 style={{ margin: '10rem auto', fontWeight: 'bold' }}>The resource you are looking for was not found!</h2>  
      </TabPanel>    
    </Tabs>      
  );
};

export default NotFound;
