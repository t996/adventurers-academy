import 'react-tabs/style/react-tabs.css';
import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { CourseProvider } from '../../hooks/courseContext';
import BranchesTabPanel from '../models/branches/BranchesTabPanel';
import CourseTabPanel from '../models/courses/CourseTabPanel';
import ReviewTabPanel from '../models/courses/ReviewTabPanel';
import SectionsTabPanel from '../models/sections/SectionsTabPanel';
import UsersTabPanel from '../models/users/UsersTabPanel';


const ManageCourse = ({ match }) => {
  const id = +match.params.id;
 
  return (
    <CourseProvider courseId={id}>
      <Tabs defaultIndex={1}>
        <TabList>
          <Tab>Review</Tab>
          <Tab>Course</Tab>
          <Tab>Branches</Tab>
          <Tab>Sections</Tab>
          <Tab>Students</Tab>
        </TabList>

        <TabPanel>
          <ReviewTabPanel />         
        </TabPanel>

        <TabPanel>
          <CourseTabPanel />
        </TabPanel>

        <TabPanel>
          <BranchesTabPanel />
        </TabPanel>
        
        <TabPanel>
          <SectionsTabPanel />
        </TabPanel>
        
        <TabPanel>
          <UsersTabPanel />
        </TabPanel>
      </Tabs>
    </CourseProvider>
  );
};

export default ManageCourse;