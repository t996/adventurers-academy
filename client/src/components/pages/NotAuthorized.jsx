import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';


const NotAuthorized = () => {
  return (
    <Tabs>
      <TabList>
        <Tab>Error</Tab>      
      </TabList>      

      <TabPanel>
        <h2 style={{ margin: '10rem auto', fontWeight: 'bold' }}> You are not authorized to view this resource!</h2>  
      </TabPanel>    
    </Tabs>      
  );
};

export default NotAuthorized;