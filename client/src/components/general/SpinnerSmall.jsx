import React from 'react';


const SpinnerSmall = () => {
  return (
    <div className="lds-small-container">
      <div className="lds-small-dual-ring"></div>
    </div>
  );
};

export default SpinnerSmall;