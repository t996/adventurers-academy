import React from 'react';
import PropTypes from 'prop-types';
import { useFormContext } from 'react-hook-form';


const CheckboxList = ({ name, listItems, onChange }) => {
  const { register } = useFormContext(); // retrieve all hook methods
  
  
  return (
    <div className="check-list-container">
      {listItems.map((option, index) => (
        <label className="form-check-label academy-field" key={option.id}>
          <input
            className="form-check-input"
            type="checkbox"
            value={option.id}
            defaultChecked={option.checked}
            {...register(name, {

            })}
          />
          {option.name}
        </label>
        
      ))}
    </div>
  );
};

CheckboxList.propTypes = {
  name: PropTypes.string.isRequired,
  listItems: PropTypes.array.isRequired,
  onChange: PropTypes.func,  
};

export default CheckboxList;