import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Spinner from './Spinner';


const ListContainer = ({ filter, setFilter, getData, ListComponent, ListAction, listActionProps, hidePageButtons }) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [isNextDisabled, setIsNextDisabled] = useState(true);
  const [isPrevDisabled, setIsPrevDisabled] = useState(true);


  const changePage = (direction) => {
    
    switch(direction) {
    case 'next':
      if (filter && filter.limit) {
        let newOffset = filter.offset + filter.limit;

        setIsPrevDisabled(newOffset === 0);

        const newFilter = {...filter, offset: newOffset};
        setFilter(newFilter);
      } 
      break;
    case 'prev':
      if (filter && filter.limit) {
        let newOffset = filter.offset - filter.limit;
        newOffset = newOffset < 0 ? 0 : newOffset; //TODO: Do something about out of bounds exceptions
        
        setIsPrevDisabled(newOffset === 0);
        
        const newFilter = {...filter, offset: newOffset};
        setFilter(newFilter);
      } 
      break;
    default:
      break;
    }
  };

  useEffect(() => {

    if (!filter) {
      return;
    }

    let unmounted = false;

    setLoading(true);
    
    getData(filter)
      .then((data) => {
        if (!unmounted) {
          if (filter.limit === data.length) {
            setIsNextDisabled(false);
          } else {
            setIsNextDisabled(true);
          }

          if (filter.offset === 0) {
            setIsPrevDisabled(true);
          }
          
          setData(data);
          setLoading(false);
        }
      })
      .catch((error) => {
        if (!unmounted) {
          setError(error.message);
          setLoading(false);
        }
      });
    return () => { unmounted = true; };    
  }, [filter, getData]);


  if (error) throw new Error(error);
  if (loading) return <Spinner />;


  return (
    <div className="list-container">
      {/*!hidePageButtons*/ false ?
        <div className='list-container-actions'>
          <button disabled={isPrevDisabled} onClick={() => changePage('prev')}>&lt;</button>
          <button disabled={isNextDisabled} onClick={() => changePage('next')}>&gt;</button>
        </div>
        : null}
      <div className='list'>
        {loading 
          ? <Spinner /> 
          : <ListComponent data={data} filter={filter} setFilter={setFilter} ListAction={ListAction} listActionProps={listActionProps} />}
      </div>
    </div>
  );
};

ListContainer.propTypes = {
  filter: PropTypes.object,
  setFilter: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  ListComponent: PropTypes.func.isRequired,
  ListAction: PropTypes.func.isRequired,
  listActionProps: PropTypes.object.isRequired,
  hidePageButtons: PropTypes.bool,
};

export default ListContainer;