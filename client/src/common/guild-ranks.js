
const guildRanks = {
  WOOD: {
    id: 1,
    name: 'Wood',
    description: ''
  },
  BRONZE: {
    id: 2,
    name: 'Bronze',
    description: ''
  },
  IRON: {
    id: 3,
    name: 'Iron',
    description: ''
  },
  SILVER: {
    id: 4,
    name: 'Silver',
    description: ''
  },
  GOLD: {
    id: 5,
    name: 'Gold',
    description: ''
  },
  PLATINUM: {
    id: 6,
    name: 'Platinum',
    description: ''
  },
};

export default guildRanks;