const userRoles = {
  TEACHER: 1,
  STUDENT: 2,
  GUEST: 3,
};

export default userRoles;