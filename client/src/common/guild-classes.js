
const guildClasses = {
  WARRIOR: {
    id: 1,
    name: 'Warrior',
    description: '',
  },
  RANGER: {
    id: 2,
    name: 'Ranger',
    description: '',
  },
  ROGUE: {
    id: 3,
    name: 'Rogue',
    description: '',
  },
  MAGE: {
    id: 4,
    name: 'Mage',
    description: '',
  },
  HEALER: {
    id: 5,
    name: 'Healer',
    description: '',    
  },
  SUPPORT: {
    id: 6,
    name: 'Support',
    description: '',
    imgName:'support'
  }
};

export default guildClasses;