import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from  'prop-types';


const GuardedRoute = ({ component: Component, auth, toPath, authorized, toAuthorizationPath, ...rest }) => (
  <Route
    {...rest} 
    render={(props) => {

      if (!auth) return <Redirect to={toPath} />;

      if (!authorized) return <Redirect to={toAuthorizationPath} />;
      
      return <Component {...props} />;
    }}
  />
);
  
GuardedRoute.propTypes = {
  component: PropTypes.func.isRequired,
  auth: PropTypes.bool.isRequired,
};
  
export default GuardedRoute;