import { createContext } from 'react';

const AuthContext = createContext({
  user: null,
  setLoggedUser: () => {},
});


export default AuthContext;