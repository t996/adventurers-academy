
//whatever we return from the reducer becomes the new state
const courseReducer = (courseData, action) => {
  switch (action.type) {
  case 'init': {
    const { data } = action;
    return data;
  }
  case 'edit': {
    const { data: { title, description, isPrivate }} = action;
    const modifiedData = { ...courseData, title, description, isPrivate };
    return modifiedData;
  }     
  case 'updateSection': {
    const { data: { id, title, isEmbedded } } = action;
    const { sections } = courseData;
    const index = sections.findIndex(s => s.id === id);
    sections[index] = { id, title, isEmbedded };

    const modifiedData = { ...courseData, sections };
    return modifiedData;
  }
  case 'createSection': {
    const { data: { id, title, isEmbedded } } = action;
    const { sections } = courseData;
    
    sections.unshift({ id, title, isEmbedded }); // put the new section at the front

    const modifiedData = { ...courseData, sections };
    return modifiedData;
  }
  case 'updateBranch': {
    const { data: { id, title, isPrivate, sections } } = action;
    const { branches } = courseData;
    const index = branches.findIndex(b => b.id === id);
    const mappedSections = sections.map(s => {return { position: s.position, timeLock: s.timeLock, section: s.section.id }; });
    branches[index] = { id, title, isPrivate, sections: mappedSections };
    const modifiedData = { ...courseData, branches };
    return modifiedData;
  }
  case 'createBranch': {
    const { data: branch } = action;
    const { branches } = courseData;
    
    branches.push(branch);

    const modifiedData = { ...courseData, branches };
    return modifiedData;
  }
  case 'enrollUser': {
    const { user } = action;
    const { users } = courseData;
    
    if (users.find(u => u.id === user.id)) {
      return courseData; // do nothing
    }

    // add branches array to the new user
    user.branches = [];

    const modifiedData = { ...courseData, users: [...users, user] };
    return modifiedData;
  }
  case 'expelUser': {
    const { user } = action;
    const { users } = courseData;
    
    const foundIndex = users.findIndex(u => u.id === user.id);
    
    if (foundIndex === -1) {
      return courseData; // do nothing
    }

    users.splice(foundIndex, 1);

    const modifiedData = { ...courseData, users: [...users] };
    return modifiedData;
  }
  case 'addBranchToUser': {
    const { user, branchId } = action;
    const { users } = courseData;
    
    const foundIndex = users.findIndex(u => u.id === user.id);
    
    if (foundIndex === -1 || !!users[foundIndex].branches.find(b => b.id === branchId)) {
      return courseData; // do nothing
    }

    users[foundIndex].branches.push({ id: branchId });

    const modifiedData = { ...courseData };
    return modifiedData;
  }

  case 'removeBranchToUser': {
    const { user, branchId } = action;
    const { users } = courseData;
    
    const foundIndex = users.findIndex(u => u.id === user.id);
    
    if (foundIndex === -1) {
      return courseData; // do nothing
    }

    const foundBranchIndex = users[foundIndex].branches.findIndex(b => b.id === branchId);
    if (foundBranchIndex === -1) {
      return courseData; // do nothing
    }

    users[foundIndex].branches.splice(foundBranchIndex, 1);

    const modifiedData = { ...courseData };
    return modifiedData;
  }

  default:
    throw new Error('Unhandled action ' + action.type);  
  }
};

export default courseReducer;