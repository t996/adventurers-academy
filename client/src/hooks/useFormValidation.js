import { useState } from 'react';


const useFormValidation = (form, setForm) => {

  const [isFormValid, setIsFormValid] = useState(false);
  const [validationMsg, setValidationMsg] = useState('');

  const handleInputChange = (event) => {
    const { name, value, checked } = event.target;
    const updatedControl = { ...form[name] };

    updatedControl.value = form[name].type === 'checkbox' ? checked : value;
    updatedControl.touched = true;
    updatedControl.valid = isInputValid(value, updatedControl.validation);

    const updatedForm = { ...form, [name]: updatedControl };
    setForm(updatedForm);
    
    const formValid = Object.values(updatedForm).every(
      (control) => control.valid
    );

    if (!updatedControl.valid) {
      setValidationMsg(updatedControl.validation.message);
    } else {
      setValidationMsg('');
    }

    setIsFormValid(formValid);
  };

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0 && input !== '0';
    }
    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }
    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }

    return isValid;
  };


  return { isFormValid, validationMsg, handleInputChange, setValidationMsg };
};


export default useFormValidation;