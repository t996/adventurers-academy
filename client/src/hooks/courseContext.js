import React, { useReducer, useEffect, useContext, useState } from 'react';
import Spinner from '../components/general/Spinner';
import { getCourse } from '../services/http-course-requests';
import courseReducer from './courseReducer'; 


const CourseContext = React.createContext(null);

export const CourseProvider = (props) => {
  
  const courseId = props.courseId;
  
  const [courseData, dispatch] = useReducer(courseReducer, null);
  const [error, setError] = useState(null);
  const [loading, setIsLoading] = useState(true);   
  
  useEffect(() => {
    let unmounted = false;

    setIsLoading(true);
    setError(null);

    const getData = async () => {

      try {
        const result = await getCourse(courseId);
        
        if (result.error) {
          throw new Error(result.message);
        }
        
        if (!unmounted) {
          dispatch({type: 'init', data: result});
          setIsLoading(false);
        }        
      }
      catch(err) {
        if (!unmounted) {
          setError(err);
          setIsLoading(false);
        }
      }
    };

    getData();
    return () => { unmounted = true; };
  }, [courseId]);
  
  const contextValue = {
    courseData,
    dispatch,
    error,
    loading
  };

  
  if (error) throw error;
  if (loading) return <Spinner />;

  
  return (
    <CourseContext.Provider value={contextValue}>
      {props.children}
    </CourseContext.Provider>
  );
};

export const useCourseData = () => {
  const context = useContext(CourseContext);
  if (!context) {
    throw new Error(
      'useCourseData must be used within a CourseProvider. Wrap a parent component in <CourseProvider> to fix this error.'
    );
  }
  return context;
};  

