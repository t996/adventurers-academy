import React, { useReducer, useEffect, useContext, useState } from 'react';
import { getToken } from '../services/auth-services';
import { getMyCourses } from '../services/http-course-requests';
import myCoursesReducer from './myCoursesReducer'; 


const MyCoursesContext = React.createContext(null);

export const MyCoursesProvider = (props) => {
  
  const userId = props.user ? props.user.id : undefined;
  const token = getToken();

  const [courseData, dispatch] = useReducer(myCoursesReducer, null);
  const [error, setError] = useState(null);
  const [loading, setIsLoading] = useState(false);   
  
  useEffect(() => {

    if (!userId || !token) {
      return;
    }
    let unmounted = false;

    setIsLoading(true);
    setError(null);

    const getData = async () => {

      try {
        const result = await getMyCourses();
        
        if (result.error) {
          throw new Error(result.message);
        }
        
        if (!unmounted) {
          dispatch({type: 'init', data: result});
          setIsLoading(false);
        }        
      }
      catch(err) {
        if (!unmounted) {
          setError(err);
          setIsLoading(false);
        }
      }
    };

    getData();
    return () => { unmounted = true; };
  }, [userId]);
  
  const contextValue = {
    courseData,
    dispatch,
    error,
    loading
  };

  
  if (error) throw error;
  // if (loading) return <Spinner />;

  
  return (
    <MyCoursesContext.Provider value={contextValue}>
      {props.children}
    </MyCoursesContext.Provider>
  );
};

export const useMyCoursesData = () => {
  const context = useContext(MyCoursesContext);
  if (!context) {
    throw new Error(
      'useMyCourseData must be used within a MyCourseProvider. Wrap a parent component in <MyCourseProvider> to fix this error.'
    );
  }
  return context;
};  

