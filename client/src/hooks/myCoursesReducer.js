
//whatever we return from the reducer becomes the new state
const myCoursesReducer = (myCoursesData, action) => {
  switch (action.type) {
  case 'init': {
    const { data } = action;
    return data;
  }
  case 'clear': {
    // const { data } = action;
    return null;
  }
  
  default:
    throw new Error('Unhandled action ' + action.type);  
  }
};

export default myCoursesReducer;