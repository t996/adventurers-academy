import { BASE_URL } from '../common/constants.js';
import { getJSONResponse, getRequestOptions } from './service-helpers.js';


export const getSection = async (id) => { 

  const response = await fetch(`${BASE_URL}/sections/${id}`, getRequestOptions('GET', true));
  return await getJSONResponse(response);
};
export const createSection = async (data) => {

  const response = await fetch(`${BASE_URL}/sections`, getRequestOptions('POST', true, 'json', data));
  return await getJSONResponse(response);
};

export const updateSection = async (id, data) => {

  const response = await fetch(`${BASE_URL}/sections/${id}`, getRequestOptions('PUT', true, 'json', data));
  return await getJSONResponse(response);
};

export const deleteSection = async (id) => {

  const response = await fetch(`${BASE_URL}/sections/${id}`, getRequestOptions('DELETE', true));
  return await getJSONResponse(response);
};
