import decode from 'jwt-decode';
import userRoles from '../common/user-roles';
import { userLogout } from './http-user-services';

export const extractUser = token => {
  
  try {
    const user = decode(token);
    return user;
  } 
  catch {
    localStorage.removeItem('token');
    return null;
  }  
};

export const getUserFromToken = (token) => {
  try {
    const user = decode(token);
    localStorage.setItem('token', token);
    return user;
  } catch {
    localStorage.removeItem('token');
  }
  return null;
};

export const getToken = () => {
  const token = localStorage.getItem('token');
  try {
    // const user = decode(token);
    decode(token);
    return token;
  } catch {
    return null;
  }
};

export const removeToken = () => {  
  localStorage.removeItem('token');
};

export const logoutUser = (setState, myCoursesDispatch) => {  
  userLogout().then(data => {
    localStorage.removeItem('token');
    setState(null);
    myCoursesDispatch({ type: 'clear'});
  });
};

export const isTeacher = () => {
  
  const token = getToken();
  
  if (!token) {
    return false;
  }
  
  const user = extractUser(token);

  return user.role === userRoles.TEACHER;
};

export const isStudent = () => {
  
  const token = getToken();
  
  if (!token) {
    return false;
  }
  
  const user = extractUser(token);

  return user.role === userRoles.STUDENT;
};