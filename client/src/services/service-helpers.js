import { getToken } from './auth-services.js';


export const getRequestOptions = (method, auth, content, data) => {

  const requestOptions = {
    method,
    headers: {},    
  };

  if (auth) {
    const token = getToken();
    if (!token) throw new Error('Unauthorized');

    requestOptions.headers.Authorization = `Bearer ${token}`;
  }

  if (content === 'json') {
    requestOptions.headers['Content-Type'] = 'application/json';
    requestOptions.body = JSON.stringify(data);
  }

  if (content === 'multipart') {
    requestOptions.body = data;
  }
  

  return requestOptions;
};

export const getJSONResponse = (response) => {
 
  if (!response.ok) {
    throw new Error(response.statusText);
  }
  
  return response.json();
};