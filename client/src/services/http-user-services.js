import { BASE_URL } from '../common/constants';
import { getJSONResponse, getRequestOptions } from './service-helpers.js';


const getQueryParams = (filter) => {
    
  if (!filter || Object.entries(filter).length === 0) {
    return '';
  }

  let result = '?';

  if (filter.query) {
    result += `search=${filter.query}&`;
  }

  if (filter.order) {
    result += `order=${filter.order}&`;
  }
  
  if (filter.direction) {
    result += `direction=${filter.direction}&`;
  }
  
  if (filter.limit) {
    result += `limit=${filter.limit}&`;
  }
  
  if (filter.offset) {
    result += `offset=${filter.offset}&`;
  }

  if (result[result.length - 1] === '&') {
    result = result.slice(0, -1);
  }

  return result;
};

export const userLogin = async (data) => {

  const response = await fetch(`${BASE_URL}/users/login`, getRequestOptions('POST', false, 'json', data));
  return getJSONResponse(response); 
};

export const userLogout = async () => {

  const response = await fetch(`${BASE_URL}/users/logout`, getRequestOptions('DELETE', true));
  return getJSONResponse(response); 
};

export const registerUser = async (data) => {

  const response = await fetch(`${BASE_URL}/users`, getRequestOptions('POST', false, 'json', data));
  return getJSONResponse(response);  
};

export const getUsers = async (filter) => {
  
  const queryParams = getQueryParams(filter);
  
  const response = await fetch(`${BASE_URL}/users${queryParams}`, getRequestOptions('GET', true));
  return getJSONResponse(response);
};

export const getUserProfile = async () => {

  const response = await fetch(`${BASE_URL}/users/profile`, getRequestOptions('GET', true));
  return getJSONResponse(response);
};

export const updateUserProfile = async (data) => {

  const response = await fetch(`${BASE_URL}/users/profile`, getRequestOptions('POST', true, 'json', data));
  return getJSONResponse(response);
};

export const updatePicture = async (data) => {

  const response = await fetch(`${BASE_URL}/users/picture`, getRequestOptions('POST', true, 'multipart', data));
  return getJSONResponse(response);
};
export const removePicture = async (data) => {

  const response = await fetch(`${BASE_URL}/users/picture`, getRequestOptions('DELETE', true));
  return getJSONResponse(response);
};