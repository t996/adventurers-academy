import { BASE_URL } from '../common/constants.js';
import { getJSONResponse, getRequestOptions } from './service-helpers.js';


const getQueryParams = (filter) => {
    
  if (!filter || Object.entries(filter).length === 0) {
    return '';
  }

  let result = '?';

  if (typeof filter.unlockAll !== 'undefined') {
    result += `unlock=${filter.unlockAll}&`;
  }


  if (filter.branchIds && filter.branchIds.length > 0) {
    result += `branches=${ Array.isArray(filter.branchIds) ? filter.branchIds.join(',') : filter.branchIds}&`;
  }

  if (filter.name) {
    result += `name=${filter.name}&`;
  }

  if (filter.query) {
    result += `search=${filter.query}&`;
  }

  if (filter.category) {
    result += `category=${filter.category}&`;
  }

  if (filter.categories) {
    result += `categories=${filter.categories.join(',')}&`;
  }

  if (filter.allowPrivate) {
    result += 'allowPrivate=true&';
  }

  if (filter.limit) {
    result += `limit=${filter.limit}&`;
  }
  if (filter.offset) {
    result += `offset=${filter.offset}&`;
  }

  if (result[result.length - 1] === '&') {
    result = result.slice(0, -1);
  }

  return result;
};


export const getCourse = async (id) => { 

  const response = await fetch(`${BASE_URL}/courses/${id}`, getRequestOptions('GET', true));
  return await getJSONResponse(response);
};

export const validateTitle = async (courseId, title) => {
  
  const response = await fetch(`${BASE_URL}/courses/validate?${courseId ? 'id=' + courseId + '&' : ''}&title=${title}`, getRequestOptions('GET', true));
  return await getJSONResponse(response);
};

export const getCourseSections = async (id) => { 

  const response = await fetch(`${BASE_URL}/courses/${id}/sections`, getRequestOptions('GET', true));
  return await getJSONResponse(response);
};

export const getCourseBranchSections = async (id, filter) => { 

  const queryParams = getQueryParams(filter);
  
  const response = await fetch(`${BASE_URL}/courses/${id}/branchsections${queryParams}`, getRequestOptions('GET', true));
  return await getJSONResponse(response);
};

export const getAllCourses = async (filter) => {  
  
  const queryParams = getQueryParams(filter);

  const response = await fetch(`${BASE_URL}/courses/all${queryParams}`, getRequestOptions('GET', true));
  return await getJSONResponse(response);
};

export const getAllCoursesPublic = async (filter) => {  
  
  const queryParams = getQueryParams(filter);

  const response = await fetch(`${BASE_URL}/courses${queryParams}`, getRequestOptions('GET', false));
  return await getJSONResponse(response);
};

export const createCourse = async (data) => {

  const response = await fetch(`${BASE_URL}/courses`, getRequestOptions('POST', true, 'json', data));
  return await getJSONResponse(response);
};

export const updateCourse = async (id, data) => {

  const response = await fetch(`${BASE_URL}/courses/${id}`, getRequestOptions('PUT', true, 'json', data));
  return await getJSONResponse(response);
};

export const deleteCourse = async (id) => {

  const response = await fetch(`${BASE_URL}/courses/${id}`, getRequestOptions('DELETE', true));
  return await getJSONResponse(response);
};


export const getMyCourses = async () => {  
  
  const response = await fetch(`${BASE_URL}/courses/my`, getRequestOptions('GET', true));
  return await getJSONResponse(response);
};

export const enrollInCourse = async (courseId) => {  
  
  const response = await fetch(`${BASE_URL}/courses/${courseId}/users`, getRequestOptions('POST', true));
  return await getJSONResponse(response);
};

export const addUserToCourse = async (courseId, userId) => {  
  
  const response = await fetch(`${BASE_URL}/courses/${courseId}/users/${userId}`, getRequestOptions('POST', true));
  return await getJSONResponse(response);
};

export const quitCourse = async (courseId) => {  
  
  const response = await fetch(`${BASE_URL}/courses/${courseId}/users`, getRequestOptions('DELETE', true));
  return await getJSONResponse(response);
};

export const removeUserFromCourse = async (courseId, userId) => {  
  
  const response = await fetch(`${BASE_URL}/courses/${courseId}/users/${userId}`, getRequestOptions('DELETE', true));
  return await getJSONResponse(response);
};