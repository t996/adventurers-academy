import { BASE_URL } from '../common/constants.js';
import { getJSONResponse, getRequestOptions } from './service-helpers.js';


export const getBranch = async (id) => { 

  const response = await fetch(`${BASE_URL}/branches/${id}`, getRequestOptions('GET', true));
  return await getJSONResponse(response);
};

export const createBranch = async (data) => {

  const response = await fetch(`${BASE_URL}/branches`, getRequestOptions('POST', true, 'json', data));
  return await getJSONResponse(response);
};

export const updateBranch = async (id, data) => {

  const response = await fetch(`${BASE_URL}/branches/${id}`, getRequestOptions('PUT', true, 'json', data));
  return await getJSONResponse(response);
};

export const deleteBranch = async (id) => {

  const response = await fetch(`${BASE_URL}/branches/${id}`, getRequestOptions('DELETE', true));
  return await getJSONResponse(response);
};

export const addAccessToBranch = async (userId, branchId) => {

  const response = await fetch(`${BASE_URL}/branches/${branchId}/users/${userId}`, getRequestOptions('POST', true));
  return await getJSONResponse(response);
};

export const removeAccessToBranch = async (userId, branchId) => {

  const response = await fetch(`${BASE_URL}/branches/${branchId}/users/${userId}`, getRequestOptions('DELETE', true));
  return await getJSONResponse(response);
};
