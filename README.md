# Adventurers Academy

## Setup

- Add .env file to the `/api` folder with the following content, replace the 'DB_USER', 'DB_PASSWORD' and 'SECRET_KEY' if appropriate.
  ```js
    PORT=3000
    DB_PORT=3306
    DB_HOST=localhost
    DB_USER=root
    DB_PASSWORD=root
    DB_CONN_LIMIT=5
    DB_QUERY_RESULT_LIMIT=10    
    DATABASE=academy
    SECRET_KEY=my_very_secret_key
    UPLOAD_LIMIT = 307200
    UPLOAD_DIR = uploads
    TEMP_DIR = temp
  ```


- Add ormconfig.json file to the `/api` folder with the following content. Change the 'username', 'password', and 'database' values if needed:

```js
{
  "type": "mysql",
  "host": "localhost",
  "port": 3306,
  "username": "root",
  "password": "root", 
  "database": "academy",
  "synchronize": true,
  "logging": false,
  "entities": [
     "src/entity/**/*.ts"
  ],
  "migrations": [
     "src/migration/**/*.ts"
  ],
  "subscribers": [
     "src/subscriber/**/*.ts"
  ]
}

```

  

  - Import into the database the latest dump file. It will create a db named 'academy' :

  

```txt
  ./stuff/db/final academy dump.sql
```
  - Run ``npm install`` in both `\api` and `\client` folders to install all the needed module packages.
  - Start the express server in the `\api directory` with `$ npm start` or `$ npm run dev` commands. It runs on port `3000`.
  - Start the react front-end client in the `\client` directory with `$ npm start` script. It runs on port `3001`.

- In the database there are pre-registered users from both roles 'teachers' and 'students' here are some of their usernames and passwords:
 
```html
teachers:
username: teacher@mail.bg  password: teacher123
username: teacher@gmail.com password: teacher123

students:
username: dwarfy@mail.bg password: 11111111

```
