import ICustomError from '../common/ICustomError';
export default class ArrayResult<T> {

  public error!: null | ICustomError;
  public data!: null | Array<T>;
  
  constructor(err: null | ICustomError, data: null | Array<T>) {
    this.error = err;
    this.data = data;
  }
}