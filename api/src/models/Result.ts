import ICustomError from '../common/ICustomError';
export default class Result<T> {

  public error!: null | ICustomError;
  public data!: null | T;
  
  constructor(err: null | ICustomError, data: null | T) {
    this.error = err;
    this.data = data;
  }
}
