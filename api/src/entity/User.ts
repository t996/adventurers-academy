import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, ManyToMany, JoinTable } from 'typeorm';
import Branch from './Branch';
import Course from './Course';
import Role from './Role';

@Entity({name: 'users'})
export default class User {

  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'int',
  })
  id!: number;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 100,
    unique: true,
    nullable: false,
  })
  email!: string;


  @Column({
    name: 'password',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  password!: string;

  @Column({
    name: 'firstName',
    type: 'varchar',
    length: 50,
    nullable: false,
    default: '',
  })
  firstName!: string;

  @Column({
    name: 'lastName',
    type: 'varchar',
    length: 50,
    nullable: false,
    default: '',
  })
  lastName!: string;

  @Column({
    name: 'guildRank',
    type: 'varchar',
    length: 50,
    nullable: false,
    default: 'bronze',
  })
  guildRank!: string;

  @Column({
    name: 'guildClass',
    type: 'varchar',
    length: 50,
    nullable: false,
    default: 'fighter',
  })
  guildClass!: string;

  @Column({
    name: 'createdOn',
    type: 'datetime',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdOn!: string;


  @Column({
    name: 'picture',
    type: 'varchar',
    length: 200,
    nullable: true,
  })
  picture!: string | null;

  //ask about this 
  @ManyToOne(() => Role, role => role.users)
  @JoinColumn({ name: 'roleId'})
  role!: Role

 
  @ManyToMany(() => Course, course => course.users, { cascade: true })
  @JoinTable({
    name: 'user_courses',
    joinColumn: {
      name: 'userId',
      referencedColumnName: 'id'
    },
    inverseJoinColumn: {
      name: 'courseId',
      referencedColumnName: 'id'
    }
  })
  public courses!: Course[];

 
  @ManyToMany(() => Branch, branch => branch.users)
  @JoinTable({
    name: 'user_branches',
    joinColumn: {
      name: 'userId',
      referencedColumnName: 'id'
    },
    inverseJoinColumn: {
      name: 'branchId',
      referencedColumnName: 'id'
    }
  })
  public branches!: Branch[];
} 
