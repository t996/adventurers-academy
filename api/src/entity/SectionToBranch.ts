import { Entity, Column, ManyToOne, PrimaryColumn, JoinColumn } from 'typeorm';
import Branch from './Branch';
import Section from './Section';


@Entity({ name: 'branch_sections'})
export default class SectionToBranch {
   
    @PrimaryColumn({
      type: 'int',
      name: 'branchId',      
    })
    @ManyToOne(() => Branch, branch => branch.id, {
      primary: true,
      nullable: false,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    })
    @JoinColumn()
    public branch!: Branch;

    @PrimaryColumn({
      type: 'int',
      name: 'sectionId',      
    })
    @ManyToOne(() => Section, section => section.id, {
      primary: true,
      nullable: false,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    })
    @JoinColumn()
    public section!: Section;

    @Column({
      name: 'position',
    })
    public position!: number;

    @Column({
      name: 'timeLock',
      nullable: true,
    })
    public timeLock!: Date;
}