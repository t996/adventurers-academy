import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({name: 'blacklist_tokens'})
export default class Token {

  @PrimaryColumn({
    name: 'token',
    type: 'varchar',
    length: 500,
    nullable: false,
  })
  token!: string;

  @Column({
    name: 'date',
    type: 'datetime',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  date!: string;
} 
