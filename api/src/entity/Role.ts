import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import User from './User';
import { STUDENT_ID, TEACHER_ID } from '../utils/config';

@Entity({name: 'roles'})
export default class Role {

  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'int',    
  })
  id!: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 50,
    unique: true,
    nullable: false,
  })
  name!: string;

  @OneToMany(() => User, user=> user.role)
  users!: User[];

  public get isTeacher(): boolean {
    return typeof TEACHER_ID !== 'undefined' && this.id === TEACHER_ID;
  }

  public get isStudent(): boolean {
    return typeof STUDENT_ID !== 'undefined' && this.id === STUDENT_ID;
  }
} 
