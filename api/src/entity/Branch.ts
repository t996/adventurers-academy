import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany, ManyToMany } from 'typeorm';
// import BranchToUser from './BranchToUser';
import Course from './Course';
import SectionToBranch from './SectionToBranch';
import User from './User';

@Entity({ 
  name: 'branches',  
})
export default class Branch {

  @PrimaryGeneratedColumn({
    name: 'id',
  })
  id!: number;

  @Column({
    name: 'title',
    type: 'varchar',
    length: 200,
    default: '',
  })
  title!: string;
  
  @Column({ 
    name: 'private',
    default: true
  })
  isPrivate!: boolean;
  
  @ManyToOne(() => Course, course => course.branches)
  @JoinColumn({ name: 'courseId' })
  course!: Course;

  @OneToMany(() => SectionToBranch, sectionToBranch => sectionToBranch.branch)
  public sections!: SectionToBranch[];

  @ManyToMany(() => User, user => user.branches)
  public users!: User[];
}