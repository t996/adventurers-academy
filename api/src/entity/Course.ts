import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany } from 'typeorm';
import Branch from './Branch';
// import CourseToUser from './CourseToUser';
import Section from './Section';
import User from './User';

@Entity({ 
  name: 'courses',  
})
export default class Course {

  @PrimaryGeneratedColumn({
    name: 'id',
  })
  id!: number;

  @Column({
    name: 'title',
    type: 'varchar',
    length: 200,
    default: '',
  })
  title!: string;
  
  @Column({
    name: 'description',
    type: 'mediumtext',
    default: '',
  })
  description!: string;

  @Column({ 
    name: 'private',
    default: true
  })
  isPrivate!: boolean;
  
  @OneToMany(() => Section, section => section.course)
  sections!: Section[];

  @OneToMany(() => Branch, branch => branch.course)
  branches!: Branch[];

  @ManyToMany(() => User, user => user.courses)
  public users!: User[];

}