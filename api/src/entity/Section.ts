import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import Course from './Course';
import SectionToBranch from './SectionToBranch';

@Entity({ 
  name: 'sections',  
})
export default class Section {

  @PrimaryGeneratedColumn({
    name: 'id',
  })
  id!: number;

  @Column({
    name: 'title',
    type: 'varchar',
    length: 200,
    default: '',
  })
  title!: string;
  
  @Column({
    name: 'content',
    type: 'mediumtext',
    default: '',
  })
  content!: string;

  @Column({ 
    name: 'embedded',
    default: true
  })
  isEmbedded!: boolean;
  
  @ManyToOne(() => Course, course => course.sections)
  @JoinColumn({ name: 'courseId' })
  course!: Course;

  @OneToMany(() => SectionToBranch, sectionToBranch => sectionToBranch.section)
  public branches!: SectionToBranch[];
}