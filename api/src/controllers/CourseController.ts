import express, { Request, Response } from 'express';
import asyncHandler from 'express-async-handler'
import Course from '../entity/Course';
import User from '../entity/User';
import userAuthentication from '../middleware/user-authentication';
import validateBody from '../middleware/validate-body';
import ICourseService from '../services/ICourseService';
import ITokenService from '../services/ITokenService';
import { sendResponse } from '../utils/handler-utilities';
import courseCreateSchema from '../validation/schemas/course-create.schema';
import courseEditSchema from '../validation/schemas/course-edit.schema';
import { TEACHER_ID } from '../utils/config';
import roleAuthorization from '../middleware/role-authorization';


class CourseController {
  public router = express.Router();

  public services: ICourseService;
  public tokenService: ITokenService;

  constructor(services: ICourseService, tokenService: ITokenService) {
    this.services = services;
    this.tokenService = tokenService;

    this.initRoutes();
  }

  public initRoutes(): void {

    this.router.route('/courses')
      .get(asyncHandler(this.getAllPublic))
      .post(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), validateBody(courseCreateSchema), asyncHandler(this.create));
  
    this.router.route('/courses/all')
      .get(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.getAll))
      
    this.router.route('/courses/my')
      .get(userAuthentication(this.tokenService), asyncHandler(this.userCourses));
    
    this.router.route('/courses/validate')
      .get(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.validateTitle));

    this.router.route('/courses/:id/users')
      .post(userAuthentication(this.tokenService), asyncHandler(this.enrollInCourse))
      .delete(userAuthentication(this.tokenService), asyncHandler(this.quitCourse));
    
    this.router.route('/courses/:id/users/:userId')
      .post(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.addUserToCourse))
      .delete(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.removeUserFromCourse));
      
    this.router.route('/courses/:id')
      .get(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.getById))
      .put(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), validateBody(courseEditSchema), asyncHandler(this.update));

    this.router.route('/courses/:id/sections')
      .get(userAuthentication(this.tokenService), asyncHandler(this.getSections));
    
    this.router.route('/courses/:id/branchSections')
      .get(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.getBranchSections));
  }

  getAll = async (req: Request, res: Response): Promise<Response> => {    
    
    const user = req.user as User;

    const result = await this.services.getAll(user);
    return sendResponse(result, req, res);
  }

  getAllPublic = async (req: Request, res: Response): Promise<Response> => {
    
    const result = await this.services.getAll(undefined);
    return sendResponse(result, req, res);
  }

  getById = async (req: Request, res: Response): Promise<Response> => {
    
    const id = +req.params.id;
    const result = await this.services.getById(id);
    return sendResponse(result, req, res);
  }

  getSections = async (req: Request, res: Response): Promise<Response> => {
    
    //get the user, must be student

    const id = +req.params.id;
    const user: User = req.user as User;

    const result = await this.services.getSections(id, user);
    return sendResponse(result, req, res);
  }

  getBranchSections = async (req: Request, res: Response): Promise<Response> => {
    
    //TODO: get the user, must be teacher
    const id = +req.params.id;
    const unlockAll = req.query.unlock === 'true' || req.query.unlock === '1';
    let branchIds: number[] = [];
   
    if (req.query.branches && typeof req.query.branches === 'string') {
      branchIds = req.query.branches.split(',').map(Number);      
    }

    const result = await this.services.getBranchSections(id, unlockAll, branchIds);
    return sendResponse(result, req, res);
  }

  create = async (req: Request, res: Response): Promise<Response> => {
    
    const course = new Course();
    course.title = req.body.title;
    course.description = req.body.description;
    course.isPrivate = req.body.isPrivate;

    const result = await this.services.create(course);
    return sendResponse(result, req, res);
  }

  update = async (req: Request, res: Response): Promise<Response> => {
    
    const course = new Course();
    course.id = +req.params.id
    course.title = req.body.title;
    course.description = req.body.description;
    course.isPrivate = req.body.isPrivate;

    const result = await this.services.update(course);
    return sendResponse(result, req, res);
  }

  validateTitle = async (req: Request, res: Response): Promise<Response> => {
    const courseId: number | undefined = req.query.id ? +req.query.id : undefined;
    const title: string | undefined = req.query.title ? req.query.title as string : undefined;

    const result = await this.services.validateTitle(courseId, title);
    return sendResponse(result, req, res);
  }
  
  userCourses = async (req: Request, res: Response): Promise<Response> => {
    const user = req.user as User;
    const result = await this.services.getCoursesByUserId(user.id);
    return sendResponse(result, req, res);
  }

  enrollInCourse = async (req: Request, res: Response): Promise<Response> => {
    const loggedUser = req.user as User;
    const courseId = +req.params.id;

    const result = await this.services.addUserToCourse(loggedUser, courseId);
    
    return sendResponse(result, req, res);
  }

  addUserToCourse = async (req: Request, res: Response): Promise<Response> => {
    const loggedUser = req.user as User;
    const courseId = +req.params.id;
    const userId = +req.params.userId;
    
    const result = await this.services.addUserToCourse(loggedUser, courseId, userId);

    return sendResponse(result, req, res);
  }

  quitCourse = async (req: Request, res: Response): Promise<Response> => {
    const loggedUser = req.user as User;
    const courseId = +req.params.id;
    
    const result = await this.services.removeUserFromCourse(loggedUser, courseId);
    
    return sendResponse(result, req, res);
  }

  removeUserFromCourse = async (req: Request, res: Response): Promise<Response> => {
    const loggedUser = req.user as User;
    const courseId = +req.params.id;
    const userId = +req.params.userId;
    
    const result = await this.services.removeUserFromCourse(loggedUser, courseId, userId);
    
    return sendResponse(result, req, res);
  }

}

export default CourseController;