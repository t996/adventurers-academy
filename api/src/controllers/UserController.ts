import express, { Request, Response } from 'express';
import asyncHandler from 'express-async-handler'
import User  from '../entity/User';
import IUserService from '../services/IUserService';
import ITokenService from '../services/ITokenService';
import CustomErrors from '../common/CustomErrors';
import userAuthentication from '../middleware/user-authentication';
import validateBody from '../middleware/validate-body';
import userRegisterSchema from '../validation/schemas/user-register.schema';
import { sendResponse } from '../utils/handler-utilities';
import Result from '../models/Result';
import Role from '../entity/Role';
import { STUDENT_ID, TEMP_DIR, UPLOAD_DIR, UPLOAD_LIMIT } from '../utils/config';
import fs from 'fs';
import path from 'path';
import { v4 as uuidv4 } from 'uuid';
import fileUpload, { UploadedFile } from 'express-fileupload';

class UserController {

  public router = express.Router();

  public services: IUserService;
  public tokenService: ITokenService;

  constructor(services: IUserService, tokenS: ITokenService) {
    this.services = services;
    this.tokenService = tokenS;
    this.initRoutes();
  }

  public initRoutes(): void {
    this.router
      .get('/users', userAuthentication(this.tokenService), asyncHandler(this.getUsers))
      .post('/users', validateBody(userRegisterSchema), asyncHandler(this.createUser))

    this.router
      .post('/users/login', asyncHandler(this.loginUser));

    this.router
      .delete('/users/logout', asyncHandler(this.logoutUser));

    this.router
      .post('/users/picture', userAuthentication(this.tokenService), fileUpload({
        limits: { fileSize: UPLOAD_LIMIT },
        abortOnLimit: true, 
        debug : true,
        useTempFiles : true,
        tempFileDir : `${TEMP_DIR}/`
      }), asyncHandler(this.updatePicture))
      .delete('/users/picture', userAuthentication(this.tokenService), asyncHandler(this.removePicture));
    
    this.router
      .get('/users/picture/:fileName', asyncHandler(this.getUserPicture));


    this.router
      .get('/users/profile', userAuthentication(this.tokenService), asyncHandler(this.getUserProfile))
      .post('/users/profile', userAuthentication(this.tokenService), asyncHandler(this.editUserProfile));

  }


  getUsers = async (req: Request, res: Response):Promise<Response> => {
    const search: string = req.query.search as string;
    
    let guildRanks: string[] | undefined = undefined;
    if (req.query.ranks) {
      guildRanks = (req.query.ranks as string).split(',');
    }

    let guildClasses: string[] | undefined = undefined;
    if (req.query.classes) {
      guildClasses = (req.query.classes as string).split(',')
    }
    
    const result = await this.services.getUsers(search, guildRanks, guildClasses);
    return sendResponse(result, req, res);    
  }


  createUser = async (req: Request, res: Response):Promise<Response> => {
    const user = new User();
    user.email = req.body.email;
    user.password = req.body.password;
    
    user.role = new Role();
    user.role.id = STUDENT_ID;
    
    const registeredUser = await this.services.createUser(user);
    const modifiedResult = new Result<string>(registeredUser.error, null);
 
    if(registeredUser.data) {
      modifiedResult.data = this.tokenService.createToken(registeredUser.data).data;
    }
    return sendResponse(modifiedResult, req, res);
  }


  loginUser = async (req: Request, res: Response):Promise<Response> => { 
    const result = await this.services.validateUser(req.body.email, req.body.password);

    const modifiedResult = new Result<string>(result.error, null);
    
    if (result.data) {
      modifiedResult.data = this.tokenService.createToken(result.data).data;
    }

    return sendResponse(modifiedResult, req, res);
  }


  logoutUser = async(req: Request, res: Response):Promise<Response> => {
    const token = req.headers.authorization?.replace('Bearer ', '');

    if (!token) {
      return sendResponse(new Result<boolean>(CustomErrors.NOT_AUTHORIZED, null), req, res);
    }

    const result = await this.tokenService.blacklistToken(token);
    return sendResponse(result, req, res);
  }


  getUserProfile = async(req: Request, res: Response):Promise<Response> => {
    const user = req.user as User;
   
    const result = await this.services.getUserById(user.id);
    return sendResponse(result, req, res);    
  }

  editUserProfile = async (req: Request, res: Response):Promise<Response> => {
    const user = req.user as User;

    const result = await this.services.editUserProfile(user.id, req.body.firstName, req.body.lastName);
    return sendResponse(result, req, res);    
  }

  
  updatePicture = async (req: Request, res: Response): Promise<Response> => {

    let fileName: string | null = null;

    if (req.files) {

      const profileFile = req.files.profilePicture as UploadedFile;

      // console.log(req.files);

      if (profileFile.mimetype !== 'image/gif' 
        && profileFile.mimetype !== 'image/jpeg' 
        && profileFile.mimetype !== 'image/png') {
        
        //remove the temp file
        if (profileFile.tempFilePath) {
          const tempPath = `${__dirname}/../../${profileFile.tempFilePath}`;
          fs.unlink(tempPath, function(err) {
            if (err) {
              console.log(err);             
            }
          });
        }

        return sendResponse(new Result<boolean>(CustomErrors.BAD_REQUEST, null), req, res);
      }

      fileName = uuidv4() + path.extname(profileFile.name);
      const dirPath = `${__dirname}/../../${UPLOAD_DIR}/`;
      const sourcePath = profileFile.tempFilePath;

      fs.rename(sourcePath, dirPath + fileName, function(err) {
        if (err) {
          return res.status(500).json(err);
        }
      });

      //delete the old picture 
      const usr = await this.services.getUserById((req.user as User).id);
      if (usr.data && usr.data.picture) {
        fs.unlink(dirPath + usr.data.picture, function(err) {
          if (err) {
            return res.status(500).json(err);
          }
        });        
      }
    }

    const result = await this.services.editUserPicture((req.user as User).id, fileName);
    return sendResponse(result, req, res); 
  }

  removePicture = async (req: Request, res: Response): Promise<Response> => {
    //delete the old picture 
    const dirPath = `${__dirname}/../../${UPLOAD_DIR}/`;
    const usr = await this.services.getUserById((req.user as User).id);
    if (usr.data && usr.data.picture) {
      fs.unlink(dirPath + usr.data.picture, function(err) {
        if (err) {
          return res.status(500).json(err);
        }
      });        
    }

    const result = await this.services.editUserPicture((req.user as User).id, null);
    return sendResponse(result, req, res); 
  }

  getUserPicture = async (req: Request, res: Response):Promise<void> => {
    
    return res.sendFile(path.resolve(`${UPLOAD_DIR}/${req.params.fileName}`))
  }
} 

export default UserController;