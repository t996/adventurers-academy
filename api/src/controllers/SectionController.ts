import express, { Request, Response } from 'express';
import asyncHandler from 'express-async-handler'
import Course from '../entity/Course';
import Section from '../entity/Section';
import User from '../entity/User';
import roleAuthorization from '../middleware/role-authorization';
import userAuthentication from '../middleware/user-authentication';
import validateBody from '../middleware/validate-body';
import ISectionService from '../services/ISectionService';
import ITokenService from '../services/ITokenService';
import { TEACHER_ID } from '../utils/config';
import { sendResponse } from '../utils/handler-utilities';
import sectionCreateSchema from '../validation/schemas/section-create.schema';
import sectionEditSchema from '../validation/schemas/section-edit.schema';


class SectionController {
  public router = express.Router();

  public services!: ISectionService;
  public tokenService!: ITokenService;

  constructor(services: ISectionService, tokenServices: ITokenService) {
    this.services = services;
    this.tokenService = tokenServices;
    
    this.initRoutes();
  }

  public initRoutes(): void {

    this.router.route('/sections')
      .post(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), validateBody(sectionCreateSchema), asyncHandler(this.create));
      
    this.router.route('/sections/:id')
      .get(userAuthentication(this.tokenService), asyncHandler(this.getById))
      .put(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), validateBody(sectionEditSchema) ,asyncHandler(this.update));
  }

  getById = async (req: Request, res: Response): Promise<Response> => {

    const id = +req.params.id;
    const user: User = req.user as User;
    
    const result = await this.services.getById(user, id);
    return sendResponse(result, req, res);
  }

  create = async (req: Request, res: Response): Promise<Response> => {
    
    const section = new Section();
    section.title = req.body.title;
    section.content = req.body.content;
    section.isEmbedded = req.body.isEmbedded;
    section.course = new Course();
    section.course.id = req.body.courseId;

    //TODO: test if course foreign key is inserted in the db

    const result = await this.services.create(section);
    return sendResponse(result, req, res); 
  }

  update = async (req: Request, res: Response): Promise<Response> => {
   
    const section = new Section();
    section.id = +req.params.id;
    section.title = req.body.title;
    section.content = req.body.content;
    section.isEmbedded = req.body.isEmbedded;
    
    //TODO: Test if course is null wether it will be removed from the db

    const result = await this.services.update(section);
    return sendResponse(result, req, res);
  }

}

export default SectionController;