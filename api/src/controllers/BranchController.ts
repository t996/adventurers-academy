import express, { Request, Response } from 'express';
import asyncHandler from 'express-async-handler'
import Course from '../entity/Course';
import Branch from '../entity/Branch';
import IBranchService from '../services/IBranchService';
import { sendResponse } from '../utils/handler-utilities';
import SectionToBranch from '../entity/SectionToBranch';
import Section from '../entity/Section';
import validateBody from '../middleware/validate-body';
import branchCreateSchema from '../validation/schemas/branch-create.schema';
import branchEditSchema from '../validation/schemas/branch-edit.schema';
import userAuthentication from '../middleware/user-authentication';
import ITokenService from '../services/ITokenService';
import roleAuthorization from '../middleware/role-authorization';
import { TEACHER_ID } from '../utils/config';


class BranchController {
   
  public router = express.Router();

  public services!: IBranchService;
  public tokenService!: ITokenService;

  constructor(services: IBranchService, tokenService: ITokenService) {
    this.services = services;
    this.tokenService = tokenService;

    this.initRoutes();
  }

  public initRoutes(): void {

    this.router.route('/branches')
      // .get(asyncHandler(this.getAll))
      .post(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), validateBody(branchCreateSchema), asyncHandler(this.create));
      
    this.router.route('/branches/:id')
      // .get(asyncHandler(this.getById))
      .put(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), validateBody(branchEditSchema), asyncHandler(this.update));

    this.router.route('/branches/:id/sections')
      .put(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.updateSections));

    this.router.route('/branches/:id/users/:uid')
      .post(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.addBranchToUser))
      .delete(userAuthentication(this.tokenService), roleAuthorization([TEACHER_ID]), asyncHandler(this.removeBranchToUser));
  }

  // getAll = async (req: Request, res: Response): Promise<Response> => {
  //   const result = await this.services.getAll();
  //   return sendResponse(result, req, res);
  // }

  // getById = async (req: Request, res: Response): Promise<Response> => {
  //   const id = +req.params.id;

  //   const result = await this.services.getById(id);
  //   return sendResponse(result, req, res);
  // }

  create = async (req: Request, res: Response): Promise<Response> => {
    
    const branch = new Branch();
    branch.title = req.body.title;
    branch.isPrivate = req.body.isPrivate;
    branch.course = new Course();
    branch.course.id = req.body.courseId;

    //TODO: test if course foreign key is inserted in the db

    const result = await this.services.create(branch);
    return sendResponse(result, req, res); 
  }

  update = async (req: Request, res: Response): Promise<Response> => {

    
    const branch = new Branch();
    branch.id = +req.params.id;
    branch.title = req.body.title;
    branch.isPrivate = req.body.isPrivate;
    branch.sections = req.body.sections;

    //add branch to all sections because undefined is not allowed
    branch.sections.forEach(s => s.branch = branch);

    //TODO: Test if course is null wether it will be removed from the db

    const result = await this.services.update(branch);
    return sendResponse(result, req, res);
  }

  updateSections = async (req: Request, res: Response): Promise<Response> => {

    const branch = new Branch();
    
    branch.id = +req.params.id;
    branch.sections = [];
    
    if (req.body.sections && req.body.sections.length > 0) {
      req.body.sections.forEach( (el: { sectionId: number, position: number, timeLock: Date | undefined }) => {
        const stb = new SectionToBranch();
        
        stb.branch = new Branch();
        stb.branch.id = branch.id;
        
        stb.section = new Section();
        stb.section.id = el.sectionId;

        stb.position = el.position;
        if (el.timeLock) {
          stb.timeLock = el.timeLock;
        }

        branch.sections.push(stb);
      });
    }
  
    const result = await this.services.updateSections(branch);
    return sendResponse(result, req, res);
  }

  addBranchToUser = async (req: Request, res: Response): Promise<Response> => {
    //const loggedUser = req.user as User;
    const branchId = +req.params.id;
    const userId = +req.params.uid;
    
    const result = await this.services.addBranchToUser(branchId, userId);

    return sendResponse(result, req, res);
  }

  removeBranchToUser = async (req: Request, res: Response): Promise<Response> => {
    //const loggedUser = req.user as User;
    const branchId = +req.params.id;
    const userId = +req.params.uid;
    
    const result = await this.services.removeBranchToUser(branchId, userId);
    
    return sendResponse(result, req, res);
  }


}

export default BranchController;