import sanitizeHtml from 'sanitize-html';

export const courseDescriptionOptions = {
  allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
};

export const sectionContentOptions = {
  allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img', 'iframe' ]),
  allowedAttributes: { ...sanitizeHtml.defaults.allowedAttributes, iframe: ['width', 'height', 'src', 'title', 'frameborder', 'allow', 'allowfullscreen']}
};

