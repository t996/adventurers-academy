export default interface ICustomError {
  code: number,
  msg: string,
}