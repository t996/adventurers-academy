import ICustomError from './ICustomError';

export default class CustomErrors {
    
  static BAD_REQUEST: ICustomError = {
    code: 400,
    msg: 'bad request',    
  };

  static NOT_AUTHORIZED: ICustomError = {
    code: 401,
    msg: 'not authorized',    
  };

  static FORBIDDEN: ICustomError = {
    code: 403,
    msg: 'forbidden',    
  };

  static NOT_FOUND: ICustomError = { 
    code: 404,
    msg: 'not found',
  };
  
  static OPERATION_NOT_ALLOWED: ICustomError = {
    code: 405,
    msg: 'operation not allowed',    
  };
  
  static DUPLICATE_RECORD: ICustomError = { 
    code: 409,
    msg: 'resource already exists',    
  };
  
  static INTERNAL_ERROR: ICustomError = { 
    code: 500,
    msg: 'internal server error',    
  };  
}