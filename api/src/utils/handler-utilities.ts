import { Request, Response } from 'express';
import ArrayResult from '../models/ArrayResult';
import Result from '../models/Result';


export const sendResponse = <T>(result: Result<T> | ArrayResult<T>, req: Request, res: Response): Response => {
  
  if (result.error) {
    return res.status(result.error.code).json(result.error.msg);
  }
  
  const successCode = req.method === 'POST' ? 201 : 200;

  return res.status(successCode).json(result.data);
};


