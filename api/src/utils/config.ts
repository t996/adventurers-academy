import * as dotenv from 'dotenv';


dotenv.config();
const path = `${__dirname}/../../.env`;
// switch (process.env.NODE_ENV) {
//   case "test":
//     path = `${__dirname}/../../.env.test`;
//     break;
//   case "production":
//     path = `${__dirname}/../../.env.prod`;
//     break;
//   default:
//     path = `${__dirname}/../../.env`;
// }
dotenv.config({ path: path });


export const PORT = process.env.PORT;
export const DB_PORT = process.env.DB_PORT;
export const DB_HOST = process.env.DB_HOST;
export const DB_USER = process.env.DB_USER;
export const DB_PASSWORD = process.env.DB_PASSWORD;
export const DATABASE = process.env.DATABASE;
export const DB_CONN_LIMIT = process.env.DB_CONN_LIMIT;
export const DB_QUERY_RESULT_LIMIT = process.env.DB_QUERY_RESULT_LIMIT;
export const SECRET_KEY = process.env.SECRET_KEY;
export const TEACHER_ID = process.env.TEACHER_ID ? +process.env.TEACHER_ID : 1;
export const STUDENT_ID = process.env.STUDENT_ID ? +process.env.STUDENT_ID : 2;
export const UPLOAD_LIMIT = process.env.UPLOAD_LIMIT ? +process.env.UPLOAD_LIMIT : 307200;
export const UPLOAD_DIR = process.env.UPLOAD_DIR;
export const TEMP_DIR = process.env.TEMP_DIR;
