import { Request, Response, NextFunction } from 'express';
import CustomErrors from '../common/CustomErrors';
import IValidationSchema from '../validation/IValidationSchema';


export default (validator: IValidationSchema) => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

  let isInvalid = false;
  validator.fields.forEach((predicate, key) => {
    if (!predicate(req.body[key])) {
      isInvalid = true;
    }
  });

  if (isInvalid) {
    return  res.status(CustomErrors.BAD_REQUEST.code).json(CustomErrors.BAD_REQUEST.msg);    
  } 

  next();
};