import { Request, Response, NextFunction } from 'express';
import CustomErrors from '../common/CustomErrors';
import User from '../entity/User';


export default (roles: number[] = []) => async (req: Request, res: Response, next: NextFunction): Promise<Response|void> => {
  const loggedUser = req.user as User;
  if (!roles.includes(loggedUser.role.id)) {
    return res.status(CustomErrors.FORBIDDEN.code).json({ message: CustomErrors.FORBIDDEN.msg });
  }

  next();
};
