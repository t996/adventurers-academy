import { Request, Response, NextFunction } from 'express';
import passport from 'passport';
import CustomErrors from '../common/CustomErrors';
import ITokenService from '../services/ITokenService';
import jwtStrategy from '../auth/strategy';


export default (tokenService: ITokenService) => async (req: Request, res: Response, next: NextFunction): Promise<Response|void> => {
 
  if (!req.headers.authorization) {
    return res.status(CustomErrors.NOT_AUTHORIZED.code).json(CustomErrors.NOT_AUTHORIZED.msg);
  }

  const token = req.headers.authorization.replace('Bearer ', '');
  const result = await tokenService.tokenExists(token); //check if the token is in the blacklist

  if (result.data) { // the token is in the blacklist
    return res.status(CustomErrors.NOT_AUTHORIZED.code).json(CustomErrors.NOT_AUTHORIZED.msg);
  } 
  passport.use(jwtStrategy)
  passport.authenticate('jwt', { session: false })(req, res, next);
};

