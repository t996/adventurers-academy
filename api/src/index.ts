import 'reflect-metadata';
import express, { Application } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import { createConnection } from 'typeorm';
import { PORT } from './utils/config';
import TokenData from './data/TokenData';
import TokenService from './services/TokenService';
import UserData from './data/UserData';
import UserService from './services/UserService';
import UserController from './controllers/UserController';
import CourseData from './data/CourseData';
import CourseService from './services/CourseService';
import CourseController from './controllers/CourseController';
import BranchData from './data/BranchData';
import BranchService from './services/BranchService';
import BranchController from './controllers/BranchController';
import SectionData from './data/SectionData';
import SectionService from './services/SectionService';
import SectionController from './controllers/SectionController';
import requestLoggerMiddleware from './logging/request-logger-middleware';
import errorLoggerMiddleware from './logging/error-logger-middleware';


const app: Application = express();

app.use(
  helmet(), 
  cors(), 
  express.json()
);

app.use(requestLoggerMiddleware);

const userData = new UserData();
const tokenData = new TokenData();
const courseData = new CourseData();
const branchData = new BranchData();
const sectionData = new SectionData();

const userService = new UserService(userData);
const tokenService = new TokenService(tokenData);
const courseService = new CourseService(courseData, branchData, sectionData, userData);
const branchService = new BranchService(branchData);
const sectionService = new SectionService(sectionData, courseService);

app.use('/', (new UserController(userService, tokenService)).router);
app.use('/', (new CourseController(courseService, tokenService)).router);
app.use('/', (new BranchController(branchService, tokenService)).router);
app.use('/', (new SectionController(sectionService, tokenService)).router);
app.all('*', (req, res) => res.status(404).send({ message: 'Resource not found!' }));


app.use(errorLoggerMiddleware);

createConnection().then(() => {    
  console.log('🗄️  App is connected to db');
  app.listen(PORT, () => console.log(`⚡️ App is listening on port ${PORT}`));
}).catch(error => console.log(error));
