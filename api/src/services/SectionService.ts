import ISectionData from '../data/ISectionData';
import ISectionService from './ISectionService';
import Section from '../entity/Section';
import Result from '../models/Result';
import CustomErrors from '../common/CustomErrors';
import User from '../entity/User';
import ICourseService from './ICourseService';
import Course from '../entity/Course';
import SectionToBranch from '../entity/SectionToBranch';
import sanitizeHtml from 'sanitize-html';
import { sectionContentOptions } from '../common/SanitizeOptions';


export default class SectionService implements ISectionService {

  private dataProvider: ISectionData;
  private courseService: ICourseService;

  constructor(provider: ISectionData, courseService: ICourseService) {
    this.dataProvider = provider;
    this.courseService = courseService;
  }

  async getById(user: User, id: number): Promise<Result<Section>> {

    const section = await this.dataProvider.getById(id);

    if (!section) {
      return new Result<Section>(CustomErrors.NOT_FOUND, null);
    }
    
    if (!user.role.isTeacher) {
      const enrolledCourses = await this.courseService.getCoursesByUserId(user.id);
      const course: Course | undefined = (enrolledCourses.data as Course[]).find(c => c.id === section.course.id);
 
      if (!course) {
        return new Result<Section>(CustomErrors.FORBIDDEN, null);
      }

      const courseSections = await this.courseService.getSections(course.id, user);
      const foundSection: SectionToBranch | undefined = (courseSections.data as SectionToBranch[]).find(s => s.section.id === section.id);

      if (!foundSection || (foundSection.timeLock && foundSection.timeLock > new Date())) {
        return new Result<Section>(CustomErrors.FORBIDDEN, null);
      }
    }

    return new Result<Section>(null, section);
  }

  async create (section: Section): Promise<Result<Section>> {
    
    section.content = sanitizeHtml(section.content, sectionContentOptions);

    const result = await this.dataProvider.create(section);

    if (!result) {
      return new Result<Section>(CustomErrors.INTERNAL_ERROR, null);
    } else {
      return new Result<Section>(null, result);
    }
  }

  async update (section: Section): Promise<Result<Section>> {

    section.content = sanitizeHtml(section.content, sectionContentOptions);

    const result = await this.dataProvider.update(section);

    if (!result) {
      return new Result<Section>(CustomErrors.INTERNAL_ERROR, null);
    } else {
      return new Result<Section>(null, result);
    }
  }
}
