// import CourseToUser from '../entity/CourseToUser';
import User from '../entity/User';
import ArrayResult from '../models/ArrayResult';
import Result from '../models/Result';

interface IUserService {

  getUsers(search: string | undefined, guildRanks: string[] | undefined, guildClasses: string[] | undefined): Promise<ArrayResult<User>>;
  
  createUser(user: User): Promise<Result<User>>;

  getUserByUsername(username: string): Promise<Result<User>>;

  validateUser(username: string, password: string): Promise<Result<User>>;

  getUserById(userId: number): Promise<Result<User>>;

  // getUserCourses(userId: number): Promise<Result<CourseToUser[]>>;

  editUserProfile(userId: number, first: string, last: string): Promise<Result<User>>;
  
  editUserPicture(userId: number, fileName: string | null): Promise<Result<User>>
}

export default IUserService;