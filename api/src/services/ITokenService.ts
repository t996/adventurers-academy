import Token  from '../entity/Token';
import User  from '../entity/User';
import Result from '../models/Result';


interface ITokenService {

  createToken(user: User): Result<string>;

  tokenExists(token: string): Promise<Result<Token>>;

  blacklistToken(token: string): Promise<Result<boolean>>;

}

export default ITokenService;