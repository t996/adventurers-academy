import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import ITokenService from './ITokenService';
import  User  from '../entity/User';
import Result from '../models/Result';
import ITokenData from '../data/ITokenData';
import CustomErrors from '../common/CustomErrors';
import Token  from '../entity/Token';


const SECRET_KEY = dotenv.config().parsed?.SECRET_KEY;

class TokenService implements ITokenService {

  public tokenDataLayer: ITokenData;

  constructor(dataLayer: ITokenData) {
    this.tokenDataLayer = dataLayer;
  }


  createToken(user: User): Result<string> {
    const payload = {
      id: user.id,
      username: user.email,
      role: user.role.id,
    }
    const token = jwt.sign(payload, SECRET_KEY as string/*, {expiresIn: '24h'}*/);
    return new Result(null, token);
  }

  async blacklistToken(token: string): Promise<Result<boolean>> {
    await this.tokenDataLayer.blacklist(token);
    return new Result<boolean>(null, true);
  }

  async tokenExists(token: string): Promise<Result<Token>> {
    const result = await this.tokenDataLayer.tokenExists(token);
    
    if (!result) {
      return new Result<Token>(CustomErrors.NOT_FOUND, null);
    } else {
      return new Result<Token>(null, result);
    }
  }
}


export default TokenService;

