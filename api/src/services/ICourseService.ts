import Course from '../entity/Course';
import SectionToBranch from '../entity/SectionToBranch';
import User from '../entity/User';
import ArrayResult from '../models/ArrayResult';
import Result from '../models/Result';


export default interface ICourseService {
  getAll(user: User | undefined): Promise<ArrayResult<Course>>;
  getById(id: number): Promise<Result<Course>>;
  create (course: Course): Promise<Result<Course>>;
  update (course: Course): Promise<Result<Course>>;

  validateTitle(courseId: number | undefined, title: string | undefined): Promise<Result<boolean>>;
  getSections(id: number, user: User): Promise<ArrayResult<SectionToBranch>>;
  getBranchSections(courseId: number, unlockAll: boolean, branchIds: number[]): Promise<ArrayResult<SectionToBranch>>;

  getCoursesByUserId(userId: number): Promise<ArrayResult<Course>>;

  addUserToCourse(loggedUser: User, courseId: number, studentId?: number): Promise<ArrayResult<Course>>
  removeUserFromCourse(loggedUser: User, courseId: number, studentId?: number): Promise<ArrayResult<Course>>
}

