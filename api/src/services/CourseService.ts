import ICourseService from './ICourseService';
import ICourseData from '../data/ICourseData';
import Course from '../entity/Course';
import Result from '../models/Result';
import ArrayResult from '../models/ArrayResult';
import CustomErrors from '../common/CustomErrors';
import sanitizeHtml from 'sanitize-html';
import Branch from '../entity/Branch';
import User from '../entity/User';
import IBranchData from '../data/IBranchData';
import ISectionData from '../data/ISectionData';
import SectionToBranch from '../entity/SectionToBranch';
import { courseDescriptionOptions } from '../common/SanitizeOptions';
import IUserData from '../data/IUserData';

export default class CourseService implements ICourseService {

  public dataProvider: ICourseData;
  public branchDataProvider: IBranchData;
  public sectionDataProvider: ISectionData;

  public userDataProvider: IUserData

  constructor(provider: ICourseData, branchData: IBranchData, sectionData: ISectionData, userData: IUserData) {
    this.dataProvider = provider;
    this.branchDataProvider = branchData;
    this.sectionDataProvider = sectionData;
    this.userDataProvider = userData;
  }

  filterSections(branches: Branch[], unlockAll: boolean): SectionToBranch[] {
    const maxPosition = branches.reduce((currentMax, branch) => {
      const branchMax = branch.sections.reduce((currentBranchMax, sb) => currentBranchMax < sb.position ? sb.position : currentBranchMax, 0);

      return currentMax < branchMax ? branchMax : currentMax;
    }, 0); 

    
    const resultList: SectionToBranch[] = [];
    const now = unlockAll ? new Date(8640000000000000) : new Date();
    for (let i = 0; i <= maxPosition; i++) {
      branches.forEach(branch => {
        const bs = branch.sections.find(s => s.position === i);

        if (bs) {
         
          if (!bs.section.isEmbedded || (bs.timeLock && bs.timeLock > now)) {
            bs.section.content = '';
          }
          const foundIndex = resultList.findIndex(item => item.section.id === bs.section.id);
          
          if (foundIndex > -1) {
            // we have a conflict and must decide which one of the sections will stay in the list
            // Logic: If both branch sections are unlocked the first one in the list stays
            //        If both are locked at this time the one that unlock soonest stays 
            //        If the first is unlocked but the second is locked then the first one stays
            //        If the first one is locked but the second is not the first is removed and the second inserted on the lower position
            
            // So we remove the first and add the second only when the first is currently locked and the second is unlocked or will unlock sooner than the first
            // otherwise we do nothing and the first stays where it is

            const previouslyInsertedBS = resultList[foundIndex];
            if (previouslyInsertedBS.timeLock && previouslyInsertedBS.timeLock > now) {
              if (!bs.timeLock || previouslyInsertedBS.timeLock > bs.timeLock) {
                resultList.splice(foundIndex, 1);
                resultList.push(bs);
              }
            }
          } else {
            resultList.push(bs);
          }
        }
      });
    }

    return resultList;
  } 

  async getAll(user: User | undefined): Promise<ArrayResult<Course>> {    

    const getPrivateCoursesToo = typeof user !== 'undefined' && user.role.isTeacher;
    
    const result = await this.dataProvider.getAll(getPrivateCoursesToo);
    
    return new ArrayResult<Course>(null, result);
  }

  async getById(id: number): Promise<Result<Course>> {
    const result = await this.dataProvider.getById(id);
  
    if(!result) {
      return new Result<Course>(CustomErrors.NOT_FOUND, null);
    } else {
      return new Result<Course>(null, result);
    }
  }

  async create (course: Course): Promise<Result<Course>> {
   
    course.description = sanitizeHtml(course.description, courseDescriptionOptions);
    
    const result = await this.dataProvider.create(course);

    if(!result) {
      return new Result<Course>(CustomErrors.INTERNAL_ERROR, null);
    } else {
      return new Result<Course>(null, result);
    }
  }

  async update (course: Course): Promise<Result<Course>> {

    course.description = sanitizeHtml(course.description, courseDescriptionOptions);

    const result = await this.dataProvider.update(course);

    if(!result) {
      return new Result<Course>(CustomErrors.INTERNAL_ERROR, null);
    } else {
      return new Result<Course>(null, result);
    }
  }

  async validateTitle(courseId: number | undefined, title: string | undefined): Promise<Result<boolean>>{

    if (!title) {
      return new Result<boolean> (null, false);
    }

    const foundCourse = await this.dataProvider.getByTitle(title);
    
    if (foundCourse) {
      if (courseId && foundCourse.id === courseId) {
        //the course title is unique because it is the same course
        return new Result<boolean> (null, true);
      } 
      
      // we have a diff course with the same title
      return new Result<boolean> (null, false);
    }

    // the title is unique 
    return new Result<boolean> (null, true);

  }

  async getSections(courseId: number, user: User): Promise<ArrayResult<SectionToBranch>> {
    
    const fullUser = await this.userDataProvider.getUserById(user.id);

    if (!fullUser || (fullUser.role.isStudent && !fullUser.courses.find(c => c.id === courseId))) {
      return new ArrayResult<SectionToBranch>(CustomErrors.FORBIDDEN, null);
    }

    let branchIds: number[] = []; 
    const course = await this.dataProvider.getById(courseId);
   
    if (!course) {
      return new ArrayResult<SectionToBranch>(CustomErrors.NOT_FOUND, null);
    } else {
      //get all the public branches
      const publicBranchIds = course.branches.filter( branch => !branch.isPrivate).map(branch => branch.id);
      //get the branches from explicit access
      const filteredExplicit = fullUser.branches.filter(b => !publicBranchIds.includes(b.id) && course.branches.find(cb => cb.id === b.id)).map(branch => branch.id);
      branchIds = [...publicBranchIds, ...filteredExplicit]
    }

    const branches = await this.branchDataProvider.getByIds(courseId, branchIds);
    
    const resultList = this.filterSections(branches, false);

    return new ArrayResult<SectionToBranch>(null, resultList);     
  }

  async getBranchSections(courseId: number, unlockAll: boolean, branchIds: number[]): Promise<ArrayResult<SectionToBranch>> {
    
    const branches = await this.branchDataProvider.getByIds(courseId, branchIds);
    
    const resultList = this.filterSections(branches, unlockAll);
    
    return new ArrayResult<SectionToBranch>(null, resultList);    
  }

  async getCoursesByUserId(userId: number): Promise<ArrayResult<Course>> {
    const result = await this.dataProvider.getCoursesByUserId(userId);
    return new ArrayResult<Course>(null, result);
  }

  async addUserToCourse(loggedUser: User, courseId: number, studentId?: number): Promise<ArrayResult<Course>> {
    
    let result = null;

    // if studentId is defined then it means a teacher is enrolling the student

    if (studentId) {
      if (!loggedUser.role.isTeacher) {
        return new ArrayResult<Course>(CustomErrors.NOT_AUTHORIZED, null);
      }

      result = await this.dataProvider.addUserToCourse(courseId, studentId);
    } else {
      const foundCourse = await this.dataProvider.getByIdLite(courseId);

      if (!foundCourse || foundCourse.isPrivate) {
        // students can't enroll into private courses by themselves
        return new ArrayResult<Course>(CustomErrors.NOT_AUTHORIZED, null);
      } else {
        result = await this.dataProvider.addUserToCourse(courseId, loggedUser.id);
      }
    }
    
    if (result === null) {
      return new ArrayResult<Course>(CustomErrors.NOT_FOUND, null);
    }
    
    return new ArrayResult<Course>(null, result);
  }

  async removeUserFromCourse(loggedUser: User, courseId: number, studentId?: number): Promise<ArrayResult<Course>> {
    let result = null;

    // if studentId is defined then it means a teacher is enrolling the student

    if (studentId) {
      if (!loggedUser.role.isTeacher) {
        return new ArrayResult<Course>(CustomErrors.NOT_AUTHORIZED, null);
      }

      result = await this.dataProvider.removeUserFromCourse(courseId, studentId);
    } else {

      result = await this.dataProvider.removeUserFromCourse(courseId, loggedUser.id);      
    }
    
    return new ArrayResult<Course>(null, result);
  }

  
}
