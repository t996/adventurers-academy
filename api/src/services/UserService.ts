import CustomErrors from '../common/CustomErrors';
import IUserData from '../data/IUserData';
import  User  from '../entity/User';
import ArrayResult from '../models/ArrayResult';
import Result from '../models/Result';
import IUserService from './IUserService';
import bcrypt from 'bcrypt';


class UserService implements IUserService {

  public usersDataLayer: IUserData;

  constructor(dataLayer: IUserData) {
    this.usersDataLayer = dataLayer;
  }


  async getUsers(search: string | undefined, guildRanks: string[] | undefined, guildClasses: string[] | undefined): Promise<ArrayResult<User>> {
    const result = await this.usersDataLayer.getUsers(search, guildRanks, guildClasses);
    return new ArrayResult(null, result);
  }

  async createUser(user: User): Promise<Result<User>> {
    const existingUser = await this.usersDataLayer.getUserByUsername(user.email);

    if (existingUser) {
      return new Result<User>(CustomErrors.DUPLICATE_RECORD, null);
    }
      
    const result = await this.usersDataLayer.createUser(user);

    if (!result) {
      return new Result<User>(CustomErrors.INTERNAL_ERROR, null);
    }

    return new Result<User>(null, result)    
  }

  async getUserByUsername(username:string): Promise<Result<User>> {
    const result = await this.usersDataLayer.getUserByUsername(username);
    if(!result) {
      return new Result<User>(CustomErrors.NOT_FOUND, null);
    } else {
      return new Result<User>(null, result);
    }
  }
  
  async validateUser(username: string, password: string): Promise<Result<User>> {
    const result = await this.usersDataLayer.getUserByUsername(username);
    if(!result) {
      return new Result<User>(CustomErrors.NOT_FOUND, null);
    } else {
      if (await bcrypt.compare(password, result.password)) {
        return new Result<User>(null, result);
      } else {
        return new Result<User>(CustomErrors.NOT_AUTHORIZED, null);
      }
    }
  }

  async getUserById(userId: number):Promise<Result<User>> {

    const result = await this.usersDataLayer.getUserById(userId);
    if(!result) {
      return new Result<User>(CustomErrors.NOT_FOUND, null);
    }
    
    return new Result<User>(null, result);
  }


  async editUserProfile(userId: number, first: string, last: string): Promise<Result<User>> {

    const result = await this.usersDataLayer.updateUserProfile(userId, first, last);
    if(result) {
      return new Result<User>(null, result);
    }
    return new Result<User>(CustomErrors.NOT_FOUND, null);
  }
  

  async editUserPicture(userId: number, fileName: string | null): Promise<Result<User>> {

    const result = await this.usersDataLayer.updateUserPicture(userId, fileName);

    if (!result) {
      return new Result<User>(CustomErrors.NOT_FOUND, null);
    }
    
    return new Result<User>(null, result);
  }

}

export default UserService;