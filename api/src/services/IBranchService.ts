import Branch from '../entity/Branch';
import Result from '../models/Result';
import ArrayResult from '../models/ArrayResult';


export default interface IBranchService {
  getAll(): Promise<ArrayResult<Branch>>;
  getById(id: number): Promise<Result<Branch>>;
  getByIds(courseId: number, ids: number[]): Promise<ArrayResult<Branch>>;

  create(branch: Branch): Promise<Result<Branch>>;
  update(branch: Branch): Promise<Result<Branch>>;
  updateSections(branch: Branch): Promise<Result<Branch>>;  

  addBranchToUser(branchId: number, userId: number): Promise<ArrayResult<Branch>>
  removeBranchToUser(branchId: number, userId: number): Promise<ArrayResult<Branch>>
}
