import IBranchData from '../data/IBranchData';
import IBranchService from './IBranchService';
import Branch from '../entity/Branch';
import Result from '../models/Result';
import ArrayResult from '../models/ArrayResult';
import CustomErrors from '../common/CustomErrors';


export default class BranchService implements IBranchService {

  private dataProvider: IBranchData;

  constructor(provider: IBranchData) {
    this.dataProvider = provider;
  }

  async getAll(): Promise<ArrayResult<Branch>> {    
    const result = await this.dataProvider.getAll();
    return new ArrayResult<Branch>(null, result);
  }

  async getById(id: number): Promise<Result<Branch>> {
    const result = await this.dataProvider.getById(id);
    
    if(!result) {
      return new Result<Branch>(CustomErrors.NOT_FOUND, null);
    } else {
      return new Result<Branch>(null, result);
    }
  }

  async getByIds(courseId: number, ids: number[]): Promise<ArrayResult<Branch>>{
    const result = await this.dataProvider.getByIds(courseId, ids);
    return new ArrayResult<Branch>(null, result);
  }

  async create (branch: Branch): Promise<Result<Branch>> {
    const result = await this.dataProvider.create(branch);

    if(!result) {
      return new Result<Branch>(CustomErrors.INTERNAL_ERROR, null);
    } else {
      return new Result<Branch>(null, result);
    }
  }

  async update (branch: Branch): Promise<Result<Branch>> {
    const result = await this.dataProvider.update(branch);

    if(!result) {
      return new Result<Branch>(CustomErrors.INTERNAL_ERROR, null);
    } else {
      return new Result<Branch>(null, result);
    }
  }

  async updateSections (branch: Branch): Promise<Result<Branch>> {
    const result = await this.dataProvider.updateSections(branch);

    if(!result) {
      return new Result<Branch>(CustomErrors.INTERNAL_ERROR, null);
    } else {
      return new Result<Branch>(null, result);
    }
  }

  async addBranchToUser(branchId: number, userId: number): Promise<ArrayResult<Branch>> {
    
    const result = await this.dataProvider.addBranchToUser(branchId, userId);
    
    return new ArrayResult<Branch>(null, result);
  }

  async removeBranchToUser(branchId: number, userId: number): Promise<ArrayResult<Branch>> {
    
    const result = await this.dataProvider.removeBranchToUser(branchId, userId);
       
    return new ArrayResult<Branch>(null, result);
  }
}
