import Section from '../entity/Section';
import Result from '../models/Result';
import User from '../entity/User';


export default interface ISectionService {
  getById(user: User, id: number): Promise<Result<Section>>;
  create(section: Section): Promise<Result<Section>>;
  update(section: Section): Promise<Result<Section>>;
}
