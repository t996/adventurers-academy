import passportJwt from 'passport-jwt';
import dotenv from 'dotenv';
import User from '../entity/User';
import Role from '../entity/Role';

const SECRET_KEY = dotenv.config().parsed?.SECRET_KEY;

const options = {
  secretOrKey: SECRET_KEY as string,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  // const user = {
  //   id: payload.id,
  //   username: payload.email,
  //   role: payload.role,
  // };
  const user = new User();
  user.id = payload.id;
  user.email = payload.username;
  user.role = new Role();
  user.role.id = payload.role;

  done(null, user);
});

export default jwtStrategy;
