import { NextFunction, Request, Response } from 'express';
import requestLogger from './request-logger';

export default async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  
  next();

  const message = {
    'Time': new Date(),
    'Request IP': req.ip,
    'Method': req.method,
    'URL': req.originalUrl,
    'statusCode': res.statusCode,
    'headers': req.headers,        
  };

  requestLogger.info(message); 
};