import { ErrorRequestHandler, Response } from 'express';
import errorLogger from './error-logger';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const errorLoggerMiddleware: ErrorRequestHandler = async (err, req, res, next): Promise<Response> => {
 
  errorLogger.error(err);
  // console.log(err);
  return res.status(500).json(err);
  
};

export default errorLoggerMiddleware;
