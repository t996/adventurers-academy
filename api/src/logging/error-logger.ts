import winston from 'winston';
import 'winston-daily-rotate-file';

const format = winston.format.combine(
  winston.format.timestamp({
    format: 'YYYY-MM-DD HH:mm:ss:ms',
  }),
  winston.format.errors({ stack: true }),
  winston.format.printf(info => `[${info.timestamp}] ${info.stack}`)  
);

const daily = new winston.transports.DailyRotateFile({
  filename: 'logs/errors-%DATE%.log',
  frequency: '1m',
  //datePattern: 'YYYY-MM-DD-HH',
  //zippedArchive: true,
  //maxSize: '20m',
  //maxFiles: '14d'
});

const transports = [
  new winston.transports.Console({
    level: 'error',
  }),
  new winston.transports.File({
    level: 'error',
    filename: 'logs/errors.log',
  }),
  daily
];

daily.on('rotate', function(oldFilename, newFilename) {
  console.log(new Date, oldFilename, newFilename);
});

export default winston.createLogger({
  format,
  transports,
});