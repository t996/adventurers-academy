import winston from 'winston';


const logConfiguration = {
  transports: [
    new winston.transports.File({
      level: 'info',
      filename: 'logs/requests.log',
      maxsize: 20*1024*1024,
    }),
  ],
  format: winston.format.combine(
    winston.format.json(),    
  ),
};

export default winston.createLogger(logConfiguration);
  
