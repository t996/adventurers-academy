import IValidationSchema from '../IValidationSchema';
import * as EmailValidator from 'email-validator';


const userRegisterSchema: IValidationSchema = {
  fields: new Map([
    ['email', (value) => EmailValidator.validate(value)],
    ['password', (value) => typeof value === 'string' && value.length > 7 && value.length < 45],
  ])
};
export default userRegisterSchema;