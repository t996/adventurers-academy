import IValidationSchema from '../IValidationSchema';


const branchCreateSchema: IValidationSchema = {
  fields: new Map([
    ['title', (value) => typeof value === 'string' && value.length >= 0 && value.length < 200],
    ['isPrivate', (value) => typeof value === 'boolean'],
    ['courseId', (value) => typeof value === 'number'],
    ['sections', (value) => Array.isArray(value) /*&& (value.length === 0 || (value.length > 0 && value.every(v => typeof v.section === 'number' && typeof v.position === 'number' && (v.timeLock === null || typeof v.timeLock.getMonth === 'function'))))*/],
  ])
};
export default branchCreateSchema;


