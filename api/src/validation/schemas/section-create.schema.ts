import IValidationSchema from '../IValidationSchema';


const sectionCreateSchema: IValidationSchema = {
  fields: new Map([
    ['title', (value) => typeof value === 'string' && value.length >= 0 && value.length < 200],
    ['content', (value) => typeof value === 'string' && value.length >= 0],
    ['isEmbedded', (value) => typeof value === 'boolean'],
    ['courseId', (value) => typeof value === 'number'],
  ])
};
export default sectionCreateSchema;