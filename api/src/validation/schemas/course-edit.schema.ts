import IValidationSchema from '../IValidationSchema';


const courseEditSchema: IValidationSchema = {
  fields: new Map([
    ['title', (value) => typeof value === 'string' && value.length >= 0 && value.length < 200],
    ['description', (value) => typeof value === 'string' && value.length >= 0],
    ['isPrivate', (value) => typeof value === 'boolean'],
  ])
};
export default courseEditSchema;