export default interface ValidationSchema {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  fields: Map<string, (value: any) => boolean>;
}