import Branch from '../entity/Branch';


export default interface IBranchData {
  getAll(): Promise<Branch[]>;
  getById(id: number): Promise<Branch | undefined>;

  getByIds(courseId: number, ids: number[]): Promise<Branch[]>;
  create(branch: Branch): Promise<Branch | undefined>;
  update(branch: Branch): Promise<Branch | undefined>;
  updateSections(branch: Branch): Promise<Branch | undefined>;

  addBranchToUser(branchId: number, userId: number): Promise<Branch[]>;
  removeBranchToUser(branchId: number, userId: number): Promise<Branch[]>
}
