import { getRepository } from 'typeorm';
import IBranchData from './IBranchData';
import User from '../entity/User';
import Branch from '../entity/Branch';
import SectionToBranch from '../entity/SectionToBranch';


export default class BranchData implements IBranchData {
  async getAll(): Promise<Branch[]> {
    
    return getRepository(Branch).createQueryBuilder('branch')
      .leftJoin('branch.course', 'course')
      .leftJoin('branch.sections', 'sections')
      .leftJoin('sections.section', 'section')
      .addSelect([
        'course.id', 
        'course.title', 
        'sections.position', 
        'sections.timeLock', 
        'section.id', 
        'section.title'])
      .getMany();
  }

  async getById(id: number): Promise<Branch | undefined> {
    
    return getRepository(Branch).findOne(id, {
      relations: ['course', 'sections', 'sections.section']
    });  
  }

  async getByIds(courseId: number, ids: number[]): Promise<Branch[]> {
    
    return getRepository(Branch).findByIds(ids, {
      relations: ['course', 'sections', 'sections.section'],
      where: { course : { id: courseId } },
      order: { id: 'ASC' },
    });    
  }

  async create(branch: Branch): Promise<Branch | undefined> {
    
    const repository = getRepository(Branch);
    const newBranch = await repository.save(branch);
    return this.getById(newBranch.id);
  }

  async update(branch: Branch): Promise<Branch | undefined> {

    const repository = getRepository(Branch);
    const updateResult = await repository.update(branch.id, { title: branch.title, isPrivate: branch.isPrivate });

    if (updateResult.affected !== 1) {
      return undefined;
    }
   
    await this.updateSections(branch);
    return this.getById(branch.id);
  }

  async updateSections(branch: Branch): Promise<Branch | undefined> {
      
    const repository = getRepository(SectionToBranch);
    
    await repository.delete({ branch: branch });
    await repository.save(branch.sections);

    return this.getById(branch.id);
  }

  async addBranchToUser(branchId: number, userId: number): Promise<Branch[]> {
    
    const userRepository = getRepository(User);

    const userWithBranches = (await userRepository.findOne(userId, { relations: ['branches'] })) as User;

    if (userWithBranches.branches.find(b => b.id === branchId)) {
      return userWithBranches.branches;
    }
    
    const newUserBranch = new Branch();
    newUserBranch.id = branchId;

    userWithBranches.branches.push(newUserBranch)
    await userRepository.save({ id: userId, branches: userWithBranches.branches });

    return userWithBranches.branches;
  }

  async removeBranchToUser(branchId: number, userId: number): Promise<Branch[]> {
    
    const userRepository = getRepository(User);

    const userWithBranches = (await userRepository.findOne(userId, { relations: ['branches'] })) as User;
    
    const filtered = userWithBranches.branches.filter(b => b.id !== branchId);

    await getRepository(User).save({ id: userId, branches: filtered });
    
    return filtered;    
  }
}
