import  Token  from '../entity/Token';


interface ITokenData {

  tokenExists(token: string): Promise<Token | undefined>;

  blacklist(token: string): Promise<Token>;

}

export default ITokenData;