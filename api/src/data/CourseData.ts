import { getRepository } from 'typeorm';
import Course from '../entity/Course';
import User from '../entity/User';
import ICourseData from './ICourseData';


export default class CourseData implements ICourseData {
  async getAll(privateToo: boolean): Promise<Course[]> {

    const query = getRepository(Course)
      .createQueryBuilder('course')
      .addSelect([
        'course.id',
        'course.title',
        'course.description',
        'course.isPrivate',
      ]);

    if (!privateToo) {
      query.where(' course.isPrivate = 0 ');
    }

    query.orderBy({
      'course.id': 'DESC',      
    });

    return query.getMany();
  }

  async getById(id: number): Promise<Course | undefined> {

    const query = getRepository(Course)
      .createQueryBuilder('course')
      .leftJoin('course.sections', 'sections')
      .leftJoin('course.branches', 'branches')
      .leftJoin('branches.sections', 'branch_sections')
      .leftJoin('course.users', 'users')
      .leftJoin('users.branches', 'user_branches')
      .addSelect([
        'sections.id', 
        'sections.title',
        'sections.isEmbedded',
        'branches',
        'branch_sections.section',
        'branch_sections.position',
        'branch_sections.timeLock',
        'users.id',
        'users.email',
        'users.firstName',
        'users.lastName',
        'users.guildRank',
        'users.guildClass',
        'users.createdOn',
        'users.picture',
        'user_branches.id'
      ])
      .where('course.id = :id  ', { id });

    // AND ( ISNULL(user_branches.courseId) || user_branches.courseId = :id )
    return query.getOne();
  }

  async getByIdLite(id: number): Promise<Course | undefined> {

    const query = getRepository(Course)
      .createQueryBuilder('course')
      .addSelect([
        'course.id', 
        'course.title',
        'course.isPrivate',        
      ])
      .where('course.id = :id', { id });

    return query.getOne();
  }

  async getByTitle(title: string): Promise<Course | undefined> {

    const query = getRepository(Course)
      .createQueryBuilder('course')
      .addSelect([
        'course.id', 
        'course.title',
        'course.isPrivate',        
      ])
      .where('course.title = :title', { title });

    return query.getOne();
  }

  async create(course: Course): Promise<Course | undefined> {
    
    return getRepository(Course).save(course);  
  }

  async update(course: Course): Promise<Course | undefined> {
    
    return getRepository(Course).save(course); 
  }

  async getCoursesByUserId(userId: number): Promise<Course[]> {
 
    return getRepository(Course).createQueryBuilder('course')
      .leftJoin('course.users', 'users')
      .addSelect([
        'course.id', 
        'course.title',
        'course.description',
        'course.isPrivate', 
      ])
      .where('users.id = :userId', { userId })
      .getMany();
  }

  async addUserToCourse(courseId: number, userId: number): Promise<Course[]> {
    
    const userCourses = await this.getCoursesByUserId(userId);

    if (!userCourses.find(c => c.id === courseId)) {
      const newCourse = new Course();
      newCourse.id = courseId;
      userCourses.push(newCourse);
      
      await getRepository(User).save({ id: userId, courses: userCourses });
    }

    return this.getCoursesByUserId(userId);    
  }

  async removeUserFromCourse(courseId: number, userId: number): Promise<Course[]> {
    
    const user = await getRepository(User).findOne(userId, { relations: ['courses', 'branches'] });
    const course = await getRepository(Course).findOne(courseId, { relations: ['branches'] });

    if (!user) { return []; }
    if (!course) { return []; }
    const filteredCourses = user.courses.filter(c => c.id !== courseId);
    const filteredBranches = user.branches.filter(b => !course.branches.find(cb => cb.id === b.id));

    await getRepository(User).save({ id: userId, courses: filteredCourses, branches: filteredBranches });
    
    return filteredCourses;    
  }

}