// import Course from '../entity/Course';
// import CourseToUser from '../entity/CourseToUser';
import User from '../entity/User';


export default interface IUserData {
  getUsers(search: string | undefined, guildRanks: string[] | undefined, guildClasses: string[] | undefined): Promise<User[]>;
  
  createUser(user: User): Promise<User | undefined>;

  getUserByUsername(username: string): Promise<User | undefined>;

  getUserById(userId: number): Promise<User | undefined>;

  // getUserCourses(userId: number): Promise<CourseToUser[] | undefined>;

  updateUserProfile(userId: number, first: string, last: string ): Promise<User | undefined>;

  updateUserPicture(userId: number, fileName: string | null): Promise<User | undefined>

}
