import Section from '../entity/Section';


export default interface ISectionData {
  getById(id: number): Promise<Section | undefined>;
  create(section: Section): Promise<Section | undefined>;
  update(section: Section): Promise<Section | undefined>;
}
