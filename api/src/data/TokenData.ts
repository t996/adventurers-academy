import { getRepository } from 'typeorm';
import Token from '../entity/Token';
import ITokenData from './ITokenData';


class TokenData implements ITokenData{

  async tokenExists(token: string): Promise<undefined | Token> {
  
    return getRepository(Token).findOne({ where: { token: token } });
  }
  
  async blacklist(token: string): Promise<Token> {
 
    const newToken = new Token()
    newToken.token = token;

    return getRepository(Token).save(newToken)    
  }

}

export default TokenData;