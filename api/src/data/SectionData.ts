import { getRepository } from 'typeorm';
// import Course from '../entity/Course';
import Section from '../entity/Section';
import ISectionData from './ISectionData';


export default class SectionData implements ISectionData {
  
  async getById(id: number): Promise<Section | undefined> {
    return await getRepository(Section).findOne(id, {relations: ['course']});
  }

  async create(section: Section): Promise<Section | undefined> {
    return await getRepository(Section).save(section);  
  }

  async update(section: Section): Promise<Section | undefined> {
    return await getRepository(Section).save(section); 
  }
}
