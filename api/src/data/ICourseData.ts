import Course from '../entity/Course';

export default interface ICourseData {
  getAll(privateToo: boolean): Promise<Course[]>;
  getById(id: number): Promise<Course | undefined>;
  getByIdLite(id: number): Promise<Course | undefined>;

  getByTitle(title: string): Promise<Course | undefined>;
  create(course: Course): Promise<Course | undefined>;
  update(course: Course): Promise<Course | undefined>;
  getCoursesByUserId(userId: number): Promise<Course[]>;

  addUserToCourse(courseId: number, userId: number): Promise<Course[]>;
  removeUserFromCourse(courseId: number, userId: number): Promise<Course[]>
}