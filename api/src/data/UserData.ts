import 'reflect-metadata';
import { getRepository, getConnection } from 'typeorm';
import User from '../entity/User';
import bcrypt from 'bcrypt';
import IUserData from './IUserData';
// import CourseToUser from '../entity/CourseToUser';


class UserData implements IUserData{

  async getUsers(search: string | undefined, guildRanks: string[] | undefined, guildClasses: string[] | undefined): Promise<User[]> {

    const query = getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.role', 'role')     
      .select([
        'user.id',
        'user.email',
        'user.firstName',
        'user.lastName',
        'user.guildRank',
        'user.guildClass',
        'user.createdOn',
        'user.picture',
        'role.id',
        'role.name',        
      ])
      .where(' role.id != 1 ');
      

    if (search) {
      query.andWhere('( LOWER(user.email) LIKE :sp OR LOWER(user.firstName) LIKE :sp OR LOWER(user.lastName) LIKE :sp )', { sp: `%${search.toLowerCase()}%` });      
    }

    if (guildRanks && guildRanks.length > 0) {
      query.andWhere('( user.guildRank IN (:...ranks) )', { ranks: guildRanks });      
    }

    if (guildClasses && guildClasses.length > 0) {
      query.andWhere('( user.guildClass IN (:...classes) )', { classes: guildClasses });      
    }

    query.orderBy({
      'user.firstName': 'ASC',
      'user.lastName': 'ASC',
    });

    return await query.getMany();    
  }


  async createUser(user: User): Promise<User | undefined> {

    const newUser = new User();
    newUser.email = user.email;
    newUser.password = await bcrypt.hash(user.password, 10);
    newUser.role = user.role;
  
    await getRepository(User).save(newUser);

    return this.getUserById(newUser.id);
  }


  async getUserByUsername(username: string): Promise<User | undefined> {

    return await getRepository(User).findOne({ where: { email: username }, relations: ['role'] });
  }


  async getUserById(userId: number): Promise<User | undefined> {
  
    const query = getRepository(User)
      .createQueryBuilder('user')
      .leftJoin('user.role', 'role')
      .leftJoin('user.branches', 'branches')
      .leftJoin('user.courses', 'courses')
      .addSelect([
        'user.id',
        'user.email',
        'user.firstName',
        'user.lastName',
        'user.guildRank',
        'user.guildClass',
        'user.createdOn',
        'user.picture',
        'role',
        'courses',
        'branches',
      ])
      .where('user.id = :id  ', { id: userId });

    const result = await query.getOne();

    if (result) {
      result.password = '';
    }

    return query.getOne();
  }

  
  async updateUserProfile(userId: number, first: string , last: string ): Promise<User | undefined> {
    
    const insertion  = await getConnection()
      .createQueryBuilder()
      .update(User)
      .set({ firstName: first, lastName: last })
      .where('id = :id', { id: userId })
      .execute();

    if (insertion.affected) {
      return await this.getUserById(userId);
    }
  }

  
  async updateUserPicture(userId: number, fileName: string | null): Promise<User | undefined> {

    const insertion = await getConnection()
      .createQueryBuilder()
      .update(User)
      .set({ picture: fileName})
      .where('id = :id', { id: userId })
      .execute();

    if (insertion.affected) {
      return await this.getUserById(userId);
    }
  }

}

export default UserData;